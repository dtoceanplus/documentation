.. _resolving-deployment-design-issues:

********************************************************
Resolving issues arising in the Deployment Design tools
********************************************************

This guide outlines possible resolutions to some issues that may arise when designing an array layout in the Deployment Design tools.
See also :ref:`dtop-troubleshooting` and :ref:`dtop-troubleshooting-portainer`.

.. contents::
   :local:
   :depth: 2

Site Characterisation
=====================

Site Characterisation fails on load shapefiles step if databases not correctly installed
----------------------------------------------------------------------------------------

The Site Characterisation module calculation will fail on the Intialize Site Characterisation Project, *Load Shapefiles* step if the SC databases were not correctly installed.
The module's log will look something like:

.. code:: text

   ****************************************
   INITIALIZE SITE CHARACTERISATION PROJECT
   ****************************************
   LOAD SHAPEFILES
   ./databases/SiteCharacterisation_User_VerificationCases-Database/Geometries/E2RM1_lease_area.shp: No such file or directory
   ./databases/SiteCharacterisation_User_VerificationCases-Database/Geometries/E2RM1_lease_area.shp: No such file or directory


The installation logfile (``install.log`` in the installation folder) may contain an entry like:

.. code:: text

    Deploying MODULE = SC
    It seems that the archive 'dtop_inst_sc_databases_v1.5.tar.gz' of the basic data files for DTOP SC module was not downloaded in advance.
    It was not found in C:\DTOceanPlus\dtop_win_inst_1.1.4 and therefore cannot be installed into C:\DTOceanPlus\dtop_win_inst_1.1.4\_volume\data/sc/databases

To resolve this, the SC databases ``tar.gz`` file needs to be moved to the installation folder and the SC module reinstalled, 
see :ref:`installing-dtoceanplus-windows10-tutorial` or :ref:`installing-dtoceanplus-linux-macos-tutorial`.

Lease area and the corridor seem not to be in the same study site
-----------------------------------------------------------------

This indicates that the lease area is distant from the export cable corridor. 
Check the inputs files are correct, with overlapping points between them, and these are set to WGS84 coordinates.

KeyError: 'times' when uploading timeseries csv
------------------------------------------------

This might indicate an issue with the date format in your csv files. It should be "year-month-day hour:minute:second", e.g. ``1980-01-01 01:00:00``.
Be careful editing csv files in Microsoft Excel, as this often reformats dates automatically.


Machine Characterisation
========================

Water depth insufficient 
---------------------------

If the water depth at your site is not sufficient, you may get an error like "An error occured while validating the input data: The sum of Hub Height and Rotor Radius (35.0 m) is larger than the average water depth (22.78 m)"

Either select a different site with a deeper water depth or set a constant depth bathymetry in the Site Characterisation tool.


Energy Delivery
================

Radial network without collection point not feasible at device voltage
----------------------------------------------------------------------

This arises because there are no suitable cables for the power of the array at the specified voltage. Either:

1. Chose a network type with a collection point to allow design of a substation with a transformer.
2. Increase the device voltage (see section below however).


Device voltage selected is higher than that output by the Energy Transformation module
--------------------------------------------------------------------------------------

If you select a higher device rated voltage in Energy Delivery than was designed in Energy Transformation you will get this warning. 
This means a device transformer should be present on the device to increase the voltage between the generator and transformer. 
This transformer is not currently designed by either the Energy Transformation or Energy Delivery modules. 
It is possible to proceed in this case, however the final design will not include a transformer onboard the device, despite one being required for the electrial network.


Station Keeping
================

Error in frequency domain analysis: equilibrium not found
---------------------------------------------------------

.. note:: TODO.

