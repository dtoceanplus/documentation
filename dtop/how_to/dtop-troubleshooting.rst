.. _dtop-troubleshooting:

**********************************************
Troubleshooting common issues with DTOceanPlus
**********************************************

This guide lists steps to troubleshoot some issues with the DTOceanPlus tools. 
See also :ref:`dtop-troubleshooting-portainer` and :ref:`resolving-deployment-design-issues`.

.. contents::
   :local:

Note that DTOceanPlus modules open in new browser tabs, so you may need to allow this in your browser (allow popups or similar). 

Cannot load a module in the browser (Error 404/page not found)
==============================================================

If you get an error message when trying to load a module (Error 404, page not found, or similar) then check that the module's services are running correctly, using the instructions below. 

In some browsers it might be necessary to set up resolving the .localhost domain as follows.

#. Type ``about:config`` in the address bar
#. In the search bar type  ``network.dns.native-is-localhost``
#. Change the value from ``false`` to ``true``

If this blocks normal internet access, just revert this setting to ``false``.

.. _dtop_mon:

Checking the status of DTOceanPlus module services
==================================================

DTOceanPlus tools use multiple services to run. If you are experiencing errors such as ``SC module currently unavailable``, it is worth checking all services are running properly. 

Using the DTOceanPlus Monitor application for Windows 
-----------------------------------------------------

The DTOceanPlus tools for Windows includes a small helper application to show the status, statistics, and information about the tools to assist with troubleshooting and debugging. 

Open the ``dtop_mon.exe`` program from the DTOceanPlus folder created during installation. 
You may recieve a warning "Windows protected your PC" about an unrecognised app, click ``More info`` and ``Run anyway``.

The application has the following functions:

1.  **Status** tab shows if all the required services are running or not. All should show a green tick next to them.

    .. image:: ../images/dtop_mon.png 

2.  **Images** tab lists all of the Docker images and details
3.  **Services** tab shows additional details about the services running for each module.
4.  **Stats** tab shows CPU and Memory usage for each service.
5.  **Info** tab shows information on the Docker configuration. Click the ``Debug Info`` button and select a filename to save a text file with debugging information. 
6.  **Backup** tab has (experimental) backup, restore, and reset functionalities. This stores backups in folders within the ``_backup`` folder of your DTOceanPlus installation folder (you may need to create this first).

    - Click ``Backup`` and enter a name (without spaces or special characters) to backup the projects and studies in your DTOceanPlus. Each module is then backed up in turn. A list of module version numbers is also saved.
    - Select a backup from the list and click ``Restore`` to load that into DTOceanPlus. This overwrites the Projects and Studies you currently have in DTOceanPlus. 
    - Click ``Reset`` to delete all DTOceanPlus projects and studies, and reinitialize DTOceanPlus. Click ``Yes`` to confirm deletion of all data. 
    - Select a backup from the list and click ``Delete`` to permanently delete that backup.
    - Click ``Refresh`` to update the list with the latest contents of the ``_backup`` folder.

By default, the DTOceanPlus Monitor is always in the foreground and minimises to the taskbar system tray (beside the clock). 
You can change this by right clicking on the icon and selecting ``Options``. 
If you get the message *DTOceanPlus Monitor is already running* when you try to open, it is minimised so double click the DTOceanPlus icon beside the clock to open it.

Using command line for Linux/macOS/Windows
------------------------------------------

The following commands may be helpful if you are experiencing issues.

- ``docker info`` gives basic information about docker configuration
- ``docker service ls`` lists the running services (replicas column should say 1/1 when running)
- ``docker stat --no-stream`` gives cpu & memory usage
- ``docker image ls --digests`` gives details of the Docker images for each module
- ``docker container ls`` lists all Docker container names
- ``Docker logs [CONTAINER NAME]`` returns the logs from the specied container


Cannot login after reinstallation
=================================

If you cannot login to the main module ``Error 502`` then close the browser tab/window and open http://mm.dtop.localhost/ in a new window. Do a full refresh (usually press ``Ctrl+F5`` keys or hold ``Ctrl`` and click refresh button, use ``Cmd/⌘`` on MacOS) to clear the browser cache.


Docker error "Docker Engine failed to start..."
===============================================

If you see the error message "Docker Engine failed to start..." when launching Docker, this may be an issue with limited memory on your computer. You can try closing other applications that might be using a lot of memory while starting Docker. 


Entity not deleted properly/cannot create a new entitiy after deleting
======================================================================

Sometimes when deleting an entity in the Main Module, it displays a mesage ``Entity was deleted`` but the ``Open``/``Delete`` buttons are still visible. 
Try refreshing the page and opening the study again, which should alow you to create a new entity.

