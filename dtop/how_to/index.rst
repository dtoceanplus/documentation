.. _dtop-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the DTOceanPlus suite of tools. 
These guides are intended for users who have previously completed all the :ref:`DTOceanPlus tutorials <dtop-tutorials>` and have a knowledge of the features of the tools. 
While the tutorials give an introduction to the basic usage of the tools, these *how-to guides* tackle slightly more advanced topics.


- :ref:`dtop-levels-of-complexity`
- :ref:`dtop-modular-architecture`
- :ref:`dtop-conventions`

- **Use the Main Module**

  - :ref:`mm-dialog-dashboard`
  - :ref:`mm-dialog-projects`
  - :ref:`mm-dialog-project`

- **Use the Catalogue Module**

  - :ref:`dtop-catalogues`
  - :ref:`cm-browse-values`

- **Troubleshooting and resolving issues**

  - :ref:`resolving-deployment-design-issues`
  - :ref:`dtop-troubleshooting`
  - :ref:`dtop-troubleshooting-portainer`

- :ref:`dtop-how-to-api`
- :ref:`dtop-gitlab-issues`
  
.. toctree::
   :maxdepth: 1
   :hidden:

   levels-of-complexity
   modular-architecture
   dtop-conventions
   
   dashboard
   projects_list
   project-studies
  
   catalogues

   resolving-deployment-design-issues
   dtop-troubleshooting
   dtop-troubleshooting-portainer

   dtop-tool-use-via-api
   dtop-gitlab-issues

.. note:: To be developed

   What other how-to guides should be added?