.. _dtop-how-to-api:

***************************************
Using the DTOceanPlus tools via the API
***************************************

.. sidebar::  Note
    
    This content was originally written as part of `EnFAIT <https://enfat.eu>`_ deliverable D10.6 DTOcean: Conclusions, 
    and has been adapted to form part of the DTOceanPlus documentation. 


The individual DTOceanPlus modules communicate via an application programming interface (API) over the 
hypertext transfer protocol (HTTP) using the OpenAPI specification. 
This means it is possible to interface directly with the 'backend' or 'business logic' that performs the 
design calculations from another piece of code or software. 
This allows for future development and expansion of the DTOceanPlus tools. 
It also permits batch running of an individual DTOceanPlus tool/module. 
This latter case was used within the assessment of electrical networks for ocean energy arrays as part of 
the `EnFAIT project <https://www.enfait.eu/>`_ by researchers from The University of Edinburgh.

The methodology of using the :ref:`Energy Delivery <ed-home>` module via the API is briefly discussed below, 
using  `MathWorks MATLAB <https://mathworks.com/>`_ software as an example, 
however similar code could be used in other programmes and languages.

.. Note:: 
    The specifics of the API for each module are described within the API Reference section of that module's 
    documentation, noting that Each module is slightly different, resulting from the varied functionality and 
    implementation. Most, however, follow a similar structure of collecting inputs, running the calculation, 
    and returning the results. 
          
.. Important::
    It is crucial to format the API request correctly, as not doing so will result in an error. 
    The cause of data formatting errors is not always immediately clear as little feedback is given. 

To better understand the API communication between the 'frontend' graphical interface and the backend, 
it is possible to use the browser's developer console (or similar name) to monitor this. 
In Google Chrome press the F12 key or select More tools > Developer tools from the  ⋮  menu in the top right. 
Then, click on the Network tab to view the HTTP requests between the frontend and backend as you use the 
DTOceanPlus module.

.. sidebar:: Links to MATLAB documentation

    - `webread function <https://www.mathworks.com/help/releases/R2022b/matlab/ref/webread.html>`_
    - `webwrite function <https://www.mathworks.com/help/releases/R2022b/matlab/ref/webwrite.html>`_ 
    - `http.RequestMessage class <https://www.mathworks.com/help/releases/R2022b/matlab/ref/matlab.net.http.requestmessage-class.html>`_

Within MATLAB, ``webread()`` function can be used to read results using the API. 
Similarly, the ``webwrite()`` function can be used to add/update inputs or to run the calculation. 
These convert data stored in a MATLAB structure to the JSON format required by the API. 
For longer calculations it might be worth using the ``http.RequestMessage`` class, as this can be set not to timeout. 

Brief code examples of these are shown below, but for full details of these functions refer to the documentation.

Example MATLAB code to communicate with the Energy Delivery backend via OpenAPI
-------------------------------------------------------------------------------

.. code:: matlab

    % check list of studies
    studies_list = webread('http://ed.dtop.localhost/api/energy-deliv-studies/');

    % Read in results for Energy Delivery study 2 into a structure ED.results
    ED.id = 2;
    url = sprintf('http://ed.dtop.localhost/api/energy-deliv-studies/%d/results', ED.id);
    ED.results = webread( url );

    % Create a structure with the updated device properties, in the format required for the API 
    ED.device.study_id = ED.id;
    ED.device.technology = 'fixed';
    ED.device.power = 250;
    ED.device.voltage = 3300;
    ED.device.connection = 'wet-mate';
    ED.device.footprint_radius = 50;
    ED.device.constant_power_factor = 1;
    ED.device.connection_point = '(0,0,0)';
    ED.device.equilibrium_draft = 0;

    % Update the device inputs for study 2 in Energy Delivery by sending the structure to the API url
    % using the PUT method to update
    url = sprintf('http://ed.dtop.localhost/api/energy-deliv-studies/%d/inputs/device’, ED.id);
    response = webwrite(url, ED.device, ...
        weboptions('MediaType','application/json', 'RequestMethod','put') );

    % Run the calculation with the updated inputs (300s timeout)
    url = sprintf('http://ed.dtop.localhost/api/energy-deliv-studies/%d/results/calculate’, ED.id);
    response = webwrite( url, weboptions('MediaType','application/json', 'Timeout',300) );

    % Alternative way to run the calculation, that will wait as needed (for longer calculations)
    url = sprintf('http://ed.dtop.localhost/api/energy-deliv-studies/%d/results/calculate’, ED.id);
    import matlab.net.http.*
    rq = RequestMessage(RequestMethod.POST, [ ...
        field.AcceptField('application/json, text/plain, */*') ...
        field.ConnectionField('keep-alive') ...
        field.ContentLengthField(0) ] );
    response = rq.send( url );

    % Add code as required to view results, or save to a structure, update other inputs, etc.
