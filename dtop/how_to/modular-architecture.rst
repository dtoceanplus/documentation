.. _dtop-modular-architecture:

Understand the modular architecture of the DTOceanPlus tools
=============================================================

The DTOceanPlus suite of tools has been develped with a modular architecture. 
Each tool can be run on it's own in standalone mode, or as part of the integrated suite.

When using the integrated suite of tools, these should be run consequtively in the specified order, as each module builds on the outptus of the previous tools. The starting point is to define a site and machine, then run through the Deployment Design tools in order. 
See also the tutorial :ref:`dtop-deployment-tools-tutorial`.
The Assessment design tools can then (optionally) be run in any order. Similarly the Stage Gate and/or Structured Innovation tools may be run before or after the Deployment and Assessment tools, as shown in :ref:`dtop-user-journeys`.

The outputs of earlier modules are used as inputs to later modules as shown in the figure below.

.. figure:: ../figures/module-linkages.svg
    :align: center

    Linkages between modules, showing which tools use outputs of others

.. note:: To be developed, with high level details of API and architecture?

          Does this also discuss Docker micro-services?
