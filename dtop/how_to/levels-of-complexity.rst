.. _dtop-levels-of-complexity:

Understand levels of complexity in DTOceanPlus
==============================================

.. draft content from D5.1 sightly updated

DTOceanPlus is designed to support the development of ocean energy technologies at all stages of the project lifecycle — from concept creation, through design development, to commercial deployment — with increasing level of data available and detail required at each. 
It is designed to support users with differing requirements in terms of detail; from investors wishing for a high-level overview of a technology or project, to developers performing more detailed technical assessments, e.g. for project consenting.
The numerical values 1, 2, 3 are also used to represent low, medium, and high complexity respectively.

.. figure:: ../figures/levels-of-complexity.svg
    :alt: Diagram showing low, medium, high complexity
    :align: center

    Levels of complexity within DTOceanPlus


In general, varying levels of complexity can be used between the different modules, however using a low complexity for one module may reduce confidence in the results of a later module. 
The complexity level for Energy Capture is linked to and defined by Machine Characterisation. 
There is no overall complexity level for Energy Transformation, instead it can be set within the module individually for each of the three stages considered. 
In some modules the calculations may not differ by complexity, but three level of complexity are used for consistency wuth other modules and allow the user to account for uncertainties in thier input data.

.. TODO add summary table of complexity per module

Project lifecycle with increasing complexity
--------------------------------------------

The project lifecycle can be seen from two complementary perspectives:

#.  The chronological phases of a project: namely conception, design, procurement, construction, installation, operation (including maintenance), and decommissioning.
#.  The project development and/or the technology deployment can be split into three stages for clarity (Early, Mid, and Late), as described in the Table below. 
    These can broadly be linked to the widely-used TRL scale [Mankins1995]_. 
    Those three stages address all the phases described above, with different levels of complexity accounted for in the project definition.

 

.. list-table::
    :header-rows: 1
    :widths: auto

    * - Stage
      - Approx. TRL
      - Development progress
      - Description
    * - Early
      - 1-3
      - Concept definition	
      - Early stage analysis of potential device or site. Gives an overview of capabilities and next development steps, but may be based on limited data.
    * - Mid
      - 4-6
      - Feasibility
      - Includes an in-depth study of the topics covered in the concept definition. More accurate than previous stage, with additional data requirements.
    * - Late
      - 7-9
      - Design and deployment
      - Key project features are planned in this stage, informed by the previous phases. Makes use of detailed information about the project.

.. Note:: Link to Stage-Gates

Level of aggregation, components to arrays
------------------------------------------

As well as being used at different stages in the technology and/or project development lifecycle, DTOceanPlus is also applicable to three different levels of aggregation, specifically:

- **Sub-system**, e.g. PTO, or moorings and foundations, that form part of a device.
 
- **Device**, i.e. one complete system that can be deployed individually or to make up an array.
 
- **Array** of multiple devices deployed in a farm or project.

Where applicable, the design tools consider aggregate details of assemblies and components. 
They are not, however, designed to assess technologies at this level of detail.

Design tool assessment method
-----------------------------

The design tools within the DTOceanPlus suite can be summarised as follows:

- The **Structured Innovation** design tool generates new concepts; including novel concepts for wave and tidal energy devices, or an improvement of a sub-system, device, or array at higher maturity level. The tool also provides the ability to assess technologies at the early concept stages when there is minimal data available and will inform part of the inputs for the Stage Gate design tool. 

- The **Stage Gate** design tool supports the objective assessment of technologies in the development process, ensuring a fair assessment of sub-systems, devices and arrays from early stage concepts up to commercial deployment. 

- The **Deployment design** tools provide optimised solutions and layouts for the deployment of ocean energy technologies, and define all the technical design specification to run the Assessment design tools for the evaluation of metrics.

- Finally, the **Assessment design** tools execute the key calculations to measure the vital parameters at all stages of the project lifecycle, and ultimately support the Stage Gate design tool by delivering these fundamental computations.

Therefore, an important functionality of DTOceanPlus is the ability to assess the performance of technologies throughout the project lifecycle, as a technology matures; when there is little to no data available about a technology at the concept definition stage, and more data from testing and simulations at the design and deployment stage. 

The Table below outlines how the assessment method changes through these different stages, depending on the data available, and introduces the terminology of ‘basic’ and ‘advanced’ modes for the tools.  
This assessment is a key functional requirement of the software, and will have consistency in the approach through integration of the tools provided by the Digital Representation. 
As a running theme throughout the project lifecycle, assessment of sub-systems, devices and arrays must be flexible to the users’ requirements depending on the particular user type, the maturity of the technology and the amount of data available. 
This is highlighted in the use cases described in section 2.2 of D2.2 Functional requirements and metrics of 2nd generation design tools [D22]_

.. list-table:: Assessment methods
    :header-rows: 1
    :widths: 1 2 4 1

    * - Stage & approx.TRL
      - Data availability
      - Assessment method
      - Tool description
    * - Early stage (TRL 1–3)
      - Little quantitative data available; overview of capabilities and operating modes
      - Assessment through the Structured Innovation and Stage Gate design tools by utilising the earliest level assessments of technologies; these may use:
          * Fundamental physics, engineering and economic relationships.
          * Simple, high level quantitative assessments from the Assessment and Deployment design tools.
          * Scoring of a technology by qualitative assessment from an expert assessor.
      - Fundamental 
    * - Mid stage (TRL 4–6)
      - *Low complexity*; limited data available
      - Simple, high level ‘basic’ quantitative assessments through the Deployment and Assessment design tools. These can be the same as the detailed ‘advanced’ tools but with simple parameters and/or default values used.  
      - Basic mode(s)
    * - Late stage (TRL 7-9)
      - *Full complexity*; makes use of detailed information about the project.
      - Detailed ‘advanced’ quantitative assessments through the Deployment and Assessment design tools.
      - Advanced mode




  



References
~~~~~~~~~~

.. [Mankins1995] Mankins, John C (1995) 
    Technology readiness levels. NASA.

.. [D22] Noble *et al.* (2018)
    DTOceanPlus D2.2 Functional requirements and metrics of 2nd generation design tools
