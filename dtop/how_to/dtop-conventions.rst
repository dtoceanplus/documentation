.. _dtop-conventions:

****************************************************************
Coordinate systems and direction conventions used in DTOceanPlus
****************************************************************

Differing conventions can be used to define coordinate systems and direction conventions for bathymetry and environmental conditions (winds, waves, and tidal currents).
Those used in DTOceanPlus are summarised below. 

Positions
---------

There are various ways to represent **horizontal positions** on the Earth's surface, with two common standards used by DTOceanPlus modules:

- *World Geodetic System 1984 (WGS84)* specifies the latitude and longitude coordinates as degrees north and degrees east.
  These are given as decimal degrees (1.2345°), with south of the equator and west of the prime meridian negative.
  Note that these may be specified elsewhere as degrees, minutes, and seconds of arc (e.g. 1°15'4.2"). The global positioning system (GPS) uses WGS84 coordinates.

- *Universal Transverse Mercator (UTM)* is an x,y grid projection, with coordinates specified in meters east and meters north for 6° wide zones around the Earth either side of the equator. 
  Most of the UK, Spain, and Western France is within zone 30N, Ireland, Western Spain, and Portugal are in zone 29N, much of the North Sea is in zone 31N, and Denmark and Western Norway are in zone 32N. 
  To avoid negative coordinates, the central meridian of the zone is defined as 500,000m east. The equator is 0m for the northern hemisphere zones. 

.. sidebar:: Local coordinate systems

    In the UK, the Ordnance Survey National Grid (OS NG) is also used, which is similar to UTM and overlaps with UTM zones 29-31. However, it has different origins and a slightly different scale factor, see [OSNG20]_ for further details. 
    Other countries and regions have different local coordinate systems.

The modules  considering the site bathymetry (EC, ED, SK) all use UTM coordinates for their calculations. 
This allows simplified rectangular grids, with little discrepancy over the scope of a site. 
The modules with a broader spatial coverage (SC, LMO, ESA) all use WGS84, as the curvature of the Earth's surface becomes more important at larger scales. 
It is possible to directly convert between coordinate systems with an acceptably high degree of accuracy, but all approximate the true shape of and position on the Earth. 


**Vertical distance** can be measured relative to any datum, nominally mean sea level. 
A timeseries can be added to represent the tide level relative to this, optionally accounting for an offset to a country, local, or chart datum.
The convention within Site Characterisation is that depths are positive (into the ocean). 
However, Energy Delivery and Station Keeping use the opposite, with a negative z coordinate (i.e. upwards positive).

Environment
-----------

**Waves** are specified in terms of significant wave height :math:`H_s` and peak period :math:`T_p`,  
with directions described using degrees from north in a 'coming-from' (Meteorological) convention. 

**Current** velocities are specified in m/s, with directions described using degrees from north in a 'going-to' (Oceanographical) convention. 
For some cases, the eastwards (zonal) and northwards (meridional) velocity :math:`u,v` components are used.

**Wind speeds** are measured in m/s at a reference height of 10m, and as with waves, the direction uses a 'coming-from' (Meteorological) convention.

References
----------

.. [OSNG20] Ordnance Survey, "A guide to coordinate systems in Great Britain," Southhampton, UK, 2020. https://www.ordnancesurvey.co.uk/documents/resources/guide-coordinate-systems-great-britain.pdf