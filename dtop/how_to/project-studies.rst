.. _mm-dialog-project:

******************************
Main Module Projects & Studies
******************************

The project page is composed of several tabs. Each tab is dedicated to a specific feature:

* *Users*: manage access rights to the project.

.. image:: ../images/mm-project-users.png
  :alt: Project page
  :align: center

* *Standalone*: manage studies and entities for the one or more tools in standalone mode.

.. image:: ../images/mm-projects-standalone.png
  :alt: Project page
  :align: center

* *Design & Assessmnt*: manage studies and entities using the full suite of tools.

.. image:: ../images/mm-projects-design-assessment.png
  :alt: Site & Machine
  :align: center


To activate a module, click on corresponding ``Create`` button, to create a new entity.
A dialog, specific to each tool, is displayed.

.. image:: ../images/mm-entity.png
  :alt: Create Entity
  :scale: 80%
  :align: center

The user will give a name to the entity and validate by clicking the ``Create``
button. When the dialog is closed, the ``Create`` button is replaced by
two buttons.

To access the module, click the ``Open`` button. The module will be
opened in a new tab of the browser. Once you have finished to use the module
close the tab to come back to the study.

If you want to restart the module, you can delete the current results by clicking
the ``Delete`` button.
