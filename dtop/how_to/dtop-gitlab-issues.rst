.. _dtop-gitlab-issues:

*****************************************************************
Reporting potential errors, bugs, or improvements to the code
*****************************************************************

.. sidebar::  Note
    
    This content was originally written as part of `EnFAIT <https://enfat.eu>`_ deliverable D10.6 DTOcean: Conclusions, 
    and has been adapted to form part of the DTOceanPlus documentation. 


If a DTOceanPlus user finds a problem with the software, or they have a suggestion for a new feature or other 
improvement to the code, they can log this in the 
`issues section of the project GitLab repository <https://gitlab.com/groups/dtoceanplus/-/issues>`_.

The first step is to search existing issues, to check if a similar point has been raised before.
When reporting potential errors or bugs, there are good practice steps that should be followed, based on [BugZilla]_ guidelines. 
Remember that 'open-source' does not mean there is an obligation for the developers to fix any bugs identified. 

The more information you can give to help the developer, the more likely the error will be resolved in a timely manner.

1. Figure out the steps to reproduce a bug:

   - If you have precise steps to reproduce, this results in a useful bug report.
   - If you can reproduce occasionally, but not after following specific steps, 
     you should try to provide additional information for the bug to be useful. 
   - If you can't reproduce the problem, there's probably no use in reporting it, 
     unless you provide unique information about its occurrence. 

2. Enter a clear unique summary as the title, using approximately 10 words. 

3. Write precise steps to reproduce. If a developer cannot reproduce the bug, it is unlikely to be fixed. 
   If the steps are unclear, it might not even be possible to know whether the bug has been fixed. 
   Try not to use vague language like just “it crashes” or “it doesn't work”.

   - Describe your method of interacting with DTOceanPlus in addition to the intent of each step. 
     Be as explicit and verbose as possible, for example state whether you pressed enter or clicked a button after 
     entering an input, as this might be part of the issue.
   - After your steps, precisely describe the observed (actual) result and the expected result. 
     Try to clearly separate facts (observations) from speculations about what the cause might be. 
     The latter may be helpful, but if not correct could waste time.
   - It may be appropriate to provide the inputs and/or outputs from previous modules to assist with debugging the error. 

4. Providing additional information, such as the operating system you are using (e.g. Windows 10 Education, version 21H2),
   and the amount of free memory (RAM) available.

The issues section can also be used to propose new features or other improvements to the software. 
Again, there is no obligation for any featured requested to be developed. 
Offering a more detailed proposal makes for a more impactful feature proposal.

The higher priority bugs and improvements identified within the detailed assessment of the DTOceanPlus tools as part of 
the `EnFAIT project <https://www.enfait.eu/>`_ have already been logged as issues within the GitLab repository. 
Where possible, further details or steps to reproduce any bugs are noted. 

.. [BugZilla] Mozilla Foundation, `Bug Writing Guidelines <https://bugzilla.mozilla.org/page.cgi?id=bug-writing.html>`_.