.. _dtop-catalogues:

**************************************
Catalogues of component/reference data
**************************************

The DTOceanPlus suite of tools include catalogues of components and reference data, 
that are introduced on this page. These components are used by the Deployment Design tools
in the design process, and by the System Lifetime Costs Assessment tool for benchmarks.


Types of component/reference data in catalogues
===============================================

The components included within the catalogues are generic representations, and do not
represent any one specific manufacturer.

The catalogues can be grouped by themes, used by the diffenent modules as follows:

- **Power Take Off Components** used by :ref:`Energy Transformation <et-home>`

  - Air turbines
  - Gearboxes
  - Hydraulic transmission systems
  - Generators
  - Power converters
  - Control systems

- **Electrical Network Equipment** used by :ref:`Energy Delivery <ed-home>` †

  - Static cables
  - Dynamic (umbilical) cables
  - Wet-mate connectors
  - Dry-mate connectors
  - Collection Points
  - Transformers

- **Mooring Line and Anchors** used by :ref:`Station Keeping <sk-home>` †

  - Mooring line properties
  - Drag anchor dimensions

- **Logistical Infrastructure and Equipment** used by :ref:`Logistics  and  Marine  Operations  (LMO) <lmo-home>` †

  - Activities
  - Speeds for different operations
  - Installation operations
  - Maintenance operations
  - Decommissioning operations
  - ROVs
  - Divers
  - Cable burial equipment 
  - Cable protection equipment
  - Pilling equipment
  - Terminals at ports
  - Vessel operations
  - Vessels

- **Cost Benchmarks** used by :ref:`System Lifetime Costs <slc-home>`

† these catalogues have been released seperately as open-source datasets, of
`electrical components <https://doi.org/10.5281/zenodo.5215642>`_, 
`mooring line and anchors <https://doi.org/10.5281/zenodo.5336289>`_, 
and `logistical infrastructure and equipment <https://doi.org/10.5281/zenodo.5222431>`_.

.. _cm-browse-values:

Browse or modify a catalogue's values
=====================================

The Catalogue Module enables the user to add, edit, and delete components within the catalogues. 
Within a catalogue, click the ``Open`` but on the right to view a specific item in the catalogue.
Click the ``Edit`` button to make changes, or ``Delete`` to remove items.

.. warning:: Changes to catalogue data can affect the consistency of the modules that are using them.

Click the ``New Value`` button and fill in the corresponding fields to add a new item to a catalogue.
Note that some precise values may be needed for successful operation of the tools.

When a catalogs as many values, it can be difficult to find a specific value. 
To simplify the search of values, the catalog page offers the following features.

Show/Hide columns
-----------------

By default, all the characteristics of the catalog's values are displayed in columns.
You can use the slider at the bottom of the list to navigate to all the column.
But it can be sometimes difficult to compare several items

You can add or remove columns using the *Show/Hide Columns* widget.

#. Click on the *Show/Hide Columns* label to expand the widget.
#. All the available characteristics of this catalog are displayed with toggle buttons.
#. Enable or disable the columns you want to show or hide.

.. image:: ../images/show_column.jpg
  :alt: Show/Hide columns
  :align: center


Use columns' filters
--------------------

You can reduce the list of values displayed by using the filters available on top of each column.

#. Type a filter in one or several columns.
#. Click the ``Filter`` button on the right.
#. The entered filters are applied, and only the values matching the requested criteria are displayed.

   .. image:: ../images/filter_columns.jpg
     :alt: Filter columns
     :align: center

#. Click the ``Reset`` button on the right to clear the filter and display all the values again.

Sort columns
------------

Use the *Up* and *Down* icons on each column to sort the list of values with this column.

.. image:: ../images/sort_columns.jpg
  :alt: Sort columns
  :align: center

Page navigation
---------------

For performances reasons, the values are displayed by pages.

Use the navigation bar at the bottom left of the page to naviage through all the values of the catalog.

The default number of values per page is *10*.

You can display more or less values by changing the value in the combo box at the bottom right of the page.

