.. _dtop-troubleshooting-portainer:

***************************************************
Using Portainer to monitor/troubleshoot DTOceanPlus
***************************************************

The Portainer Web UI can be used to control and manage DTOP deployed docker infrastructure, modules services environment, monitor application performance, detect possible problems, simplify processes and streamline operations. 
More `details in the module documentation on GitLab <https://gitlab.com/dtoceanplus/dtop_inst/-/blob/master/INSTALLATION.md#portainer-web-ui>`_.


.. _dtop-portainer-setup:

Setting up Portainer
--------------------

After (re-)installing DTOceanPlus, you must set up Portainer as follows:

1. Open the address http://portainer.dtop.localhost/ in a browser
   (or click the ``Portainer`` button at the end of the Windows installation programme).
2. Create a new (local) account for this service by typing a username and password.
3. Then Select the left button `Docker - Manage the local Docker Environment` and click `Connect`.
4. On the next page, under **Endpoints** click on `local` which may be the only option.
5. Use the left menu to select/view Stacks, Services, Containers, Images. 


Checking logs using Portainer
------------------------------

The raw logging from the Python backend for each module can be checked using the Portainer service installed alongside the tools.

1.  Open Portainer in a browser window at the address http://portainer.dtop.localhost/.
2.  Click on ``Services`` in the left menu.
3.  Browse or search for the service you want

    .. Note:: These use the module abbreviations and the Python code is run within a ``_backend`` service.
              MC, EC, and RAMS also use a ``worker`` service. 

              - ``mm_mm_``     Main Module
              - ``cm_cm_``     Catalogue Module
              - ``sc_sc_``     Site Characterisation
              - ``mc_mc_``     Machine Characterisation
              - ``ec_ec_``     Energy Capture
              - ``et_et_``     Energy Transformation
              - ``ed_ed_``     Energy Delivery 
              - ``sk_sk_``     Station Keeping 
              - ``lmo_lmo_``   Logistics and Marine Operations 
              - ``spey_spey_`` System  Performance and Energy Yield 
              - ``slc_slc_``   System Lifetime Costs
              - ``rams_rams_`` System Reliability, Availability, Maintainability, Survivability
              - ``esa_esa_``   Environmental and Social Acceptance
              - ``sg_sg_``     Stage Gate
              - ``si_si_``     Structured Innovation

4.  Click on the ``Service logs`` button to view the logs. Optionally, you can uncheck the ``Auto-refresh logs`` button, ``Download logs`` or ``copy`` the log to the clipboard.

Portainer not working after reinstallation
------------------------------------------

If the Portainer application is not working correctly after reinstalling the DTOceanPlus tools, the following steps may help:

1.  Click the ``log out`` link in the top right of the Portainer window.
2.  Do a full refresh (usually press ``Ctrl+F5`` keys or hold ``Ctrl`` and click refresh button, use ``Cmd/⌘`` on MacOS) to clear the browser cache.
3.  Set up the Portainer application again as described in :ref:`dtop-portainer-setup` above.
