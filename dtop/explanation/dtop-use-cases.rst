.. _dtop-use-cases:

Detailed use cases for DTOceanPlus
==================================

This page sets out the detailed use cases considered during the development of the suite of tools. 
These are adaped from `D2.2 Functional requirements and metrics of 2nd generation design tools <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D2.2-Functional-requirements-and-metrics-of-2nd-generation-design-tools>`_.

The DTOceanPlus suite consists of three types of design tools:

1. **Structured Innovation tool** – for creating concepts and ideas.
2. **Stage Gate tools** – a framework to support decision making.
3. **Deployment and Assessment Design tools** – for calculating parameters/metrics.


The predominant users of DTOceanPlus can also be split into three main categories:

1. **Technology Developers** – focusing on developing their specific device/technology.
2. **Project Developers** – focusing on deploying devices/arrays commercially.
3. **Public & Private Investors** – with largely overlapping requirements of understanding financial implications in support of the first two users and development of the sector. 


Other users, such as certification bodies or academics, will largely be acting in one or more of these capacities. 
It is acknowledged that this list of user types does not fully cover the full complexity of all those who may use DTOceanPlus, but it offers a useful illustrative simplification. 

A 3×3 matrix of users and tools is shown below, illustrating the process of identifying interlinking the 9 high-level use cases (UC). Within these, a total of 43 example use cases are presented in the following sections with key inputs and outputs, although it is noted this list is not exhaustive.

+----------------------------+------------------------+------------------------+------------------------+
|                            | Structured Innovation  | Stage Gate tool        | Deployment and         |
|                            | tool                   |                        | Assessment             |
| Users                      |                        |                        | design tools           |
+============================+========================+========================+========================+
| Technology Developers      | :ref:`UC1 <UC1>`       | :ref:`UC2 <UC2>`       | :ref:`UC3 <UC3>`       |
+----------------------------+------------------------+------------------------+------------------------+
| Project Developers         | :ref:`UC4 <UC4>`       | :ref:`UC5 <UC5>`       | :ref:`UC6 <UC6>`       |
+----------------------------+------------------------+------------------------+------------------------+
| Public & Private Investors | :ref:`UC7 <UC7>`       | :ref:`UC8 <UC8>`       | :ref:`UC9 <UC9>`       |
+----------------------------+------------------------+------------------------+------------------------+


.. _UC1:

UC1. Technology Developers using Structured Innovation design tools
-------------------------------------------------------------------

Inputs
    User requirements (e.g. budget, risk, location, etc…) or technology characteristics relating to existing technology
Output
    New concepts/ideas

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC1.1. | Creating new or improving a device concept              | UC1.2, UC1.3, UC2.4, UC7.1, UC7.2 |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.2. | Creating new or improving a sub-system                  | UC2.2, UC2.4, UC7.1, UC7.2        |
|        | for an existing device                                  |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.3. | Identifying enabling technologies                       | UC1.1, UC1.2, UC2.4               |
|        | required (gap analysis)                                 |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.4. | Generating ideas for optimising device:                 | UC3.1, UC3.2, UC2.4               |
|        | topology/scale(s)/location(s)/market(s)                 |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.5. | Assessing a current technology                          | UC2.1, UC7.1, UC7.2               |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.6. | Identifying and quantifying challenges                  | UC2.3                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC1.7. | Identifying potential areas of opportunity              | UC2.3                             |
+--------+---------------------------------------------------------+-----------------------------------+

.. _UC2:

UC2. Technology Developers using Stage Gate design tools
---------------------------------------------------------

Inputs
    Technology characteristics
Outputs
    Current stage; Steps to meet next stage; or an appropriate answer to the deployment and assessment design tools (energy yield etc.) depending on stage

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC2.1. | Assesses what stage their technology is                 | UC1.5                             |
|        | at including sub-systems and   devices                  |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC2.2. | Comparison with standard benchmarks/threshold           | UC1.2, UC8.1                      |
|        | (progression to next stage) (LCOE/other)                |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC2.3. | Assessing areas of compliance & non-compliance          | UC1.6, UC1.7, UC3.2               |
+--------+---------------------------------------------------------+-----------------------------------+
| UC2.4. | Identify what needs to be done to                       | UC1.1, UC1.2, UC1.3, UC1.4,       |
|        | meet the next stage                                     | UC7.1, UC7.2, UC8.2, UC8.3        |
+--------+---------------------------------------------------------+-----------------------------------+
| UC2.5. | Provide evidence for marketing/investment               | UC3.3, UC7.1, UC8.4               |
+--------+---------------------------------------------------------+-----------------------------------+

.. _UC3:

UC3. Technology Developers using Deployment and Assessment design tools
-----------------------------------------------------------------------

Inputs
    Site and technology characteristics
Outputs
    Outputs from deployment and assessment design tools (energy yield etc.)


+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC3.1. | Assess how their device/technology works                | UC1.4, UC3.2, UC3.3               |
|        | in an array cf. individual device                       |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC3.2. | Assess how their device/technology                      | UC1.4, UC2.3, UC3.3, UC5.5, UC6.1 |
|        | performs/behaves with different locations               |                                   |
|        | & balance of plant (either for single device            |                                   |
|        | or an array )                                           |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC3.3. | Optimising the size of array and balance                | UC2.5, UC3.1, UC3.2, UC6.2        |
|        | of plant for their specific device                      |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC3.4. | Provide evidence for marketing/investment               | UC9.1                             |
+--------+---------------------------------------------------------+-----------------------------------+

.. _UC4:

UC4. Project Developers using Structured Innovation design tools
----------------------------------------------------------------

Inputs
    User requirements (e.g. budget, risk, location, etc…)
Outputs
    New concepts/ideas


+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC4.1. | Creating new or improving an array concept              | UC5.3                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.2. | Identifying areas of opportunity, in terms of           | UC5.3                             |
|        | topology/scale(s)/ location(s)/market(s)                |                                   |
|        | for array/device/subsystem                              |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.3. | Identifying enabling technologies required              | UC5.3                             |
|        | (gap analysis)                                          |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.4. | Identifying types of transition points in               | UC5.2                             |
|        | terms of array size/scale                               |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.5. | Assessing current arrays/technology                     | UC5.1                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.6. | Identifying and quantifying challenges                  |                                   |
+--------+---------------------------------------------------------------------------------------------+
| UC4.7. | Identifying areas of opportunity                        |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC4.8. | To get indications on where/how to focus                | UC6                               |
|        | use of the deployment design tools                      |                                   |
+--------+---------------------------------------------------------+-----------------------------------+

.. _UC5:

UC5. Project Developers using Stage Gate design tools
-----------------------------------------------------

Inputs
    Technology and project characteristics
Outputs
    Current stage; steps to meet next stage; or an appropriate answer to the assessment design tools depending on stage

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC5.1. | Assesses what stage their project/array is at           | UC4.5                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC5.2. | Identify when to upscale (transition points)            | UC4.4                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC5.3. | Identify what needs to be done to meet                  | UC4.1, UC4.2, UC4.3,              |
|        | the next stage                                          | UC7.1, UC7.2, UC8.2               |
+--------+---------------------------------------------------------+-----------------------------------+
| UC5.4. | Assess when to move between different stages            | UC8.3                             |
|        | of development (e.g. prelim. study >                    |                                   |
|        | feasibility > detailed design)                          |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC5.5. | Assess enabling technologies and devices                | UC3.2, UC6.3                      |
|        | (acting like an investor based on outputs               |                                   |
|        | from Stage Gate Metrics)                                |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC5.6. | Provide evidence for marketing/investment               | UC8.4                             |
+--------+---------------------------------------------------------+-----------------------------------+


.. _UC6:

UC6. Project Developers using Deployment and Assessment design tools 
--------------------------------------------------------------------

Links back to UC4.8

Inputs  
    Site, technology & project characteristics
Outputs
    Suitability of device for site; outputs from deployment design tools

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC6.1. | Assess how a device/technology performs/behaves         | UC3.2                             |
|        | with different locations & balance of plant             |                                   |
|        | (either for single device or an array)                  |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC6.2. | Optimise size/scale/balance of plant in the array       | UC3.3                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC6.3. | Planning deployment and O&M                             | UC5.5                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC6.4. | Provide evidence for marketing/investment               | UC9.1                             |
+--------+---------------------------------------------------------+-----------------------------------+


.. _UC7:

UC7. Public and Private Investors using Structured Innovation design tools
--------------------------------------------------------------------------

Inputs
    User requirements (e.g. budget, risk, location, etc…)
Outputs
    Ideas for investment/funding

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC7.1. | Identify attractive areas of innovation for investment  | UC1.1, UC1.2, UC1.5, UC2.4,       |
|        |                                                         | UC2.5, UC5.3, UC8.3, UC9.3        |
+--------+---------------------------------------------------------+-----------------------------------+
| UC7.2. | (Public) Design of funding calls                        | UC1.1, UC1.2, UC1.3, UC1.7,       |
|        |                                                         | UC2.4, UC5.3, UC8.3               |
+--------+---------------------------------------------------------+-----------------------------------+


.. _UC8:

UC8. Public and Private Investors using Stage Gate design tools
---------------------------------------------------------------

Inputs
    Technology & project characteristics
Outputs
    Outputs from assessment design tools

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC8.1. | Assess projects, devices, enabling technologies and     | UC2.2                             |
|        | (based on outputs from SGM)                             |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC8.2. | (Public) Assess if device/technology ready              | UC2.4, UC5.3                      |
|        | to go to the next stage?                                |                                   |
+--------+---------------------------------------------------------+-----------------------------------+
| UC8.3. | (Public) Identify R&D opportunities                     | UC2.4, UC5.4, UC7.1, UC7.2, UC9.3 |
+--------+---------------------------------------------------------+-----------------------------------+
| UC8.4. | (Private) Assist in investment decisions                | UC2.5, UC5.5, UC9.1, UC9.2        |
+--------+---------------------------------------------------------+-----------------------------------+


.. _UC9:

UC9. Public and Private Investors Deployment and Assessment design tools
------------------------------------------------------------------------

Inputs
    Technology & project characteristics
Outputs
    Outputs from assessment design tools

+--------+---------------------------------------------------------+-----------------------------------+
| Case   | Description                                             | Links to                          |
+========+=========================================================+===================================+
| UC9.1. | Assist in investment decisions                          | UC3.4, UC6.4, UC8.4               |
+--------+---------------------------------------------------------+-----------------------------------+
| UC9.2. | Due diligence                                           | UC8.4                             |
+--------+---------------------------------------------------------+-----------------------------------+
| UC9.3. | Future potential for array expansion                    | UC7.1, UC8.3                      |
+--------+---------------------------------------------------------+-----------------------------------+

