.. dtop-technologies-and-architecture:

******************************************************
Technologies and architecture of the DTOceanPlus tools
******************************************************

.. sidebar::  Note
    
    This content was originally written as part of `EnFAIT <https://enfat.eu>`_ deliverable D10.6 DTOcean: Conclusions, 
    and has been adapted to form part of the DTOceanPlus documentation. 

There are a range of technologies that are used in the development of the DTOceanPlus tools. 
To facilitate future development of DTOceanPlus, these are summarised at a high level here, together with a description 
of the software architecture, based in part on [D7.1]_, [D7.4]_, [D7.5]_. 

The implementation for each module varies slightly, depending on the requirements of that module, and on the organisation 
that was responsible for its development.

The architecture of the DTOceanPlus suite of tools is modular; with the operation based on services. 
Each module has the following components:

- a backend business logic, which performs the calculations and design algorithms
- a frontend graphical user interface (GUI), accessed via a  web browser
- an application programming interface (API), to allow communication between frontend and backend, 
  and for communication between modules

DTOceanPlus uses the `Docker Engine <https://www.docker.com/>`_, an open-source containerisation technology for building 
and running the DTOP modules within software containers. 
As noted in [33], “a container is a standard unit of software that packages up code and all its dependencies so the 
application runs quickly and reliably from one computing environment to another”. 
This allows the DTOceanPlus tools to be run on different operating systems and computer architectures. 
The final Docker production images for each of the modules are available in the Container Registry of the public GitLab 
repositories of DTOceanPlus. These are downloaded and set up by the DTOceanPlus installation script.

The main language for the implementation of the DTOceanPlus project is Python 3.x, 
which is used for the majority of the backend business logic. 
This is an upgrade from the (now obsolete) Python v2.x used in the original DTOcean (v1.0 & v2.0) tools. 
The backend logic for some modules builds on the backend from the original DTOcean code, upgraded to Python 3.x. 
For other modules, including all new modules, the backend was written from scratch.

The frontend graphical user interface for all modules uses the `Element UI vue.js framework <https://element.eleme.io/#/en-US>`_
to provide the browser-based HTML pages to interact with the DTOceanPlus tools. 
The Flask framework is used to provide routing (pairing HTML to URL) to display information in different paths. 

Each DTOceanPlus module provides a list of services that can be used by other modules. 
The services use the Representational State Transfer (REST) approach and HTTP as the transport protocol. 
The OpenAPI language is adopted to document the available API functionalities. 
The OpenAPI file (in JSON or YAML format) indicates all the paths, services and schemas provided to other modules. 
The API and its OpenAPI documentation are automatically generated from the data models of the BL using the SAFRS framework. 
The REST API for each module is public. For the orchestration of module services in DTOceanPlus (microservices) 
Docker in SWARM mode is used. 
Traefik proxy Edge Router interacts with Docker registry/orchestrator API and generates the properly resolved routes to 
DTOceanPlus Module Services (microservices). 
These routes allow access to the Frontends and Backends of the DTOceanPlus modules, as interactive web applications 
for the users of DTOceanPlus. 
They can also be accessed directly using the API as discussed in :ref:`dtop-how-to-api`. 

.. [D7.1] F. Pons, V. Nava, and D. R. Noble, “DTOceanPlus D7.6 Final suite of design tools for devices and arrays,” DTOceanPlus Consortium, D7.6, 2021.
.. [D7.4] F. Pons, “DTOceanPlus D7.4 Handbook of Software Implementation,” DTOceanPlus Consortium, 2019.
.. [D7.5] F. Pons, E. Mei, P. Ruiz-Minguela, and V. Nava, “DTOceanPlus D7.5 Database visualisation and maintenance tool,” DTOceanPlus Consortium, 2019. 
