.. _dtoceanplus-development:

*********************************************
Development of the DTOceanPlus suite of tools
*********************************************




The DTOceanPlus suite of tools was developed as part of the DTOceanPlus 
project funded by the European Union's Horizon 2020 research and innovation
programme under grant agreement No 785921. This builds on the 
first-generation of DTOcean tools. 



DTOcean 
-------

DTOcean was a European collaborative project funded by the European 
Commission under the 7th Framework Programme for Research and Development,
under grant agreement No 608597.
DTOcean aimed at accelerating the industrial development of ocean energy 
power generation knowledge, and providing open-source design tools for 
deploying the first generation of wave and tidal energy converter arrays.

The DTOcean project gathered together 18 partners from 11 countries 
(Ireland, Spain, United Kingdom, Germany, Portugal, France, Norway, 
Denmark, Sweden, Belgium, and United States of America) under the 
coordination of the University of Edinburgh. 
The consortium partners were:

   - `The University of Edinburgh <https://www.ed.ac.uk/>`_, UK
   - `Tecnalia <https://www.tecnalia.com/en/>`_, Spain
   - `Fraunhofer Institute for Wind Energy Systems <https://www.iwes.fraunhofer.de/en.html>`_
   - `Hydraulic & Maritime Research Centre, University College Cork <https://www.marei.ie/>`_, Ireland
   - `The University of Exeter <https://www.exeter.ac.uk/>`_, UK
   - `Aalborg University <https://www.en.build.aau.dk/>`_, Denmark
   - `Marintek <http://marinetek.net/>`_, Norway
   - `WavEC <http://www.wavec.org/>`_, Portugal
   - `France Energies Marines <https://www.france-energies-marines.org/en/>`_, France
   - `Iberdrola <https://www.iberdrola.es/en>`_, Spain
   - `Vattenfall <https://group.vattenfall.com/>`_, Sweden
   - `DEME Blue Energy <https://www.deme-group.com/>`_, Belgium
   - `Prysmian <https://uk.prysmiangroup.com/>`_, Spain
   - `Tension Technologies International <https://www.tensiontech.com/>`_, UK
   - `IT Power <http://www.itpenergised.com/>`_
   - `Sandia National Laboratories <https://www.sandia.gov/>`_, USA
   - `Joint Research Centre <https://ec.europa.eu/jrc/en>`_, Belgium
   - `Ocean Energy Europe <https://www.oceanenergy-europe.eu/>`_, Belgium

Version 1.0 of the DTOcean tools was released in January 2017, and is available from https://github.com/DTOcean. 

.. Note:: This version may no longer be usable, due to historical software dependencies.

DTOcean 2.0
~~~~~~~~~~~

DTOcean 2.0 is the product of 18 months of development by Irish consultancy Data Only Greater while partnering with Sandia National Laboratories, a Department of Energy, National Nuclear Security Administration laboratory based in the United States. 
Following the release of the first version of the tool, Sandia has been helping to identify bugs in the software and evaluate its effectiveness by comparing the output to wave and tidal energy reference models.

Version 2.0 fixes several problems identified with the original release, offering full levelized cost of energy (LCoE) calculation for the first time. It also adds functionality to help developers quantify risk to profitability resulting from the unique environmental conditions faced by ocean energy technologies.

The DTOcean software package is available as a free download from https://github.com/DTOcean/dtocean. 
New video tutorials, demonstrating installation of DTOcean, are available at Data Only Greater's website https://www.dataonlygreater.com.


DTOceanPlus Project
-------------------

The DTOceanPlus project ran from May 2018 to August 2021, and developed this second generation suite of open-source design tools, building on DTOcean. 
The project, led by Tecnalia, comprised a multidisciplinary team of 16 partners from 7 EU countries, with the collaboration of 2 leading research laboratories from the USA:

    - `Tecnalia <https://www.tecnalia.com/en/>`_, Spain
    - `The University of Edinburgh <https://www.ed.ac.uk/>`_, UK
    - `Energy System Catapult <https://es.catapult.org.uk/>`_, UK 
    - `Wave Energy Scotland <https://www.waveenergyscotland.co.uk/>`_, UK
    - `France Energies Marines <https://www.france-energies-marines.org/en/>`_, France
    - `WavEC <http://www.wavec.org/>`_, Portugal
    - `Aalborg University <https://www.en.build.aau.dk/>`_, Denmark
    - `EDF <https://www.edf.fr/en/meta-home>`_, France
    - `Enel Green Power <https://www.enelgreenpower.com/>`_, Italy
    - `Naval Energies <https://www.naval-energies.com/en/>`_, France
    - `Bureau Veritas <https://www.bureauveritas.com/marine-and-offshore>`_, France
    - `Nova Innovation <https://www.novainnovation.com/>`_, UK
    - `Corpower Ocean <www.corpowerocean.com>`_, Sweden
    - `Open Cascade <https://www.opencascade.com/>`_, France
    - `Orbital Marine Power <http://orbitalmarine.com/>`_, UK
    - `IDOM Oceantec <https://www.idom.com/>`_, Spain
    - `Energias de Portugal Centre for New Energy Technologies <https://www.edp.com/en>`_, Portugal
    - `Sabella <https://www.sabella.bzh/en>`_, France
    - `Sandia National Laboratories <https://www.sandia.gov/>`_, USA
    - `National Renewable Energy Laboratory <https://www.nrel.gov/>`_, USA

Naval Energies terminated its participation on 31st August 2018 and
EDF terminated its participation on 31st January 2019.

Future Development
------------------

The DTOceanPlus suite of tools is released under an open-source 
`GNU Affero General Public License v3.0 <http://choosealicense.com/licenses/agpl-3.0/>`_ 
that allows future development of the code.

While anyone can get involved in the future development of the open-source tools, 
it is likely that at least some involvement of those responsible for the original development would be extremely beneficial.
Although the documentation of the DTOceanPlus code is comprehensive, there are still some gaps remaining.

:ref:`dtop-gitlab-issues`