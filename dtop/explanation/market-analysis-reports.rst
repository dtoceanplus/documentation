.. _market-analysis-reports:

***************************************************************
Market Analysis and Implementation Feasibility of Ocean Energy
***************************************************************


Alongside the suite of tools, the DTOceanPlus project roduced a series of reports on 
the market for Ocean Energy. 
These are briefly summarised below, with links to the public reports.

.. contents::
    :local:

D8.1. Analysis of potential markets for Ocean Energy technology
---------------------------------------------------------------

This addresses potential markets for ocean energy technology deployment and exploitation, both the present market status and future opportunities for commercialisation of grid and non-grid applications.

Prospective market opportunities for these technologies have been identified and reviewed. Although determining the potential revenue opportunities at such an early stage in development of the wave and tidal stream technologies is rather challenging, this report provides information regarding prospective or current market sizes, potential applications, geographical locations, and future outlook of the markets.

The **largest opportunity** for ocean energy technologies is the **future market for grid power**. Future scenarios for both global and EU electricity markets are given, together with projections for ocean energy growth over the short-to-medium term and the medium-to-long term.

In addition to grid power, other potential markets for ocean energy are considered. A wide range of alternative markets have been identified, based on the US DoE 'Powering the Blue Economy' study and other resources, and these can be summarised as below.

- Isolated power systems/ islands/ microgrids. 
- Offshore oil & gas extraction, processing, and decommissioning. 
- Marine aquaculture and algae. 
- Desalination. 
- Coastal resiliency and disaster recovery. 
- Ocean observation and navigation.
- Unmanned underwater vehicles. 
- Seawater and seabed mining. 
- Marine datacentres. 
  
These alternative applications may form a market for some technology developers. They may also act as a ‘stepping-stone’ to reduce costs to a level where ocean energy technologies can be cost competitive to provide grid power. Furthermore, wave and tidal stream offer an additional benefit that can be exploited for the establishment of smart local energy systems and the contribution to the development of a blue economy by enabling synergies between the potential markets identified.

Download the report `D8.1 Analysis of potential markets for Ocean Energy technology <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D8.1-Potential-Markets-for-Ocean-Energy>`_.


D8.2. Analysis of the European supply chain
--------------------------------------------

This work analyses the value chain of ocean energy, regarding its stakeholders, structure, current engagement and breakdown of project costs. It explores the mapping of the opportunities for European companies and encompasses the typical project lifecycle activities, such as project management, supply of ocean energy devices and balance of plant, as well as the installation, commissioning, operations & maintenance, and decommissioning activities.

The similarities between Offshore Wind and Ocean Energy are presented in this report and can be exploited to transfer knowledge and experience. These similarities can be found not only on the technological aspects but also on the installation, operations&maintenance, commissioning and decommissioning. Taking advantage of these potential synergies can help address the challenge related to the cost competitiveness of Ocean Energy technologies as well as encourage third parties to engage with the Ocean Energy sector and enter the value chain.

Cost competitiveness is identified as a major challenge facing the Ocean Energy sector, since the majority of the existing technologies are not yet in a commercial stage and cannot compete with other more mature renewable energy technologies. The detailed assessment of costs is still a difficult task within the sector given the scale and number of deployments to date.

Ocean Energy is bringing unique challenges to marine governance frameworks. Legal and regulatory aspects are frequently regarded as major non-technical challenges to the deployment of ocean energy, as a stable and complete policy framework for the ocean energy sector is currently missing, being currently tailored for more established uses of the sea, such as the oil and gas industry, fishing, and shipping.

Download the report `D8.2 Analysis of the European supply chain <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D8.2-Analysis-of-the-European-Supply-Chain>`_.

D8.3. Feasibility and cost-benefit analysis
--------------------------------------------

Ocean energy remains a nascent energy industry, with tidal stream technology at a pre-commercial stage and wave technology at demonstration level. These technologies require further research and development effort and significant cost reductions to partake in the highly competitive markets for grid power. Consistent cost reductions with increasing deployment have been seen in other renewable energy technologies, such as wind turbines and solar photovoltaics, and it is expected that a similar trend will be seen in future for ocean energy. Therefore, ocean energy has the opportunity to play a crucial role in the transition to net-zero, especially with the predictable nature of the tides and complementary generation profiles of wave to wind and solar.

This required reduction in the costs of ocean energy technologies could occur through some combination of two mechanisms: 

- **Incremental reductions in the Levelised Cost of Energy** (LCOE) facilitated by **subsidised deployment** of technology, and 
- **Step-change cost reductions** resulting from directed **innovation programmes**.

A range of 'what-if?' scenarios are used to illustrate the costs of different policy mixes within a range of input assumptions. These scenarios all have the target of meeting cost-parity assuming an average European wholesale market price of 50 €/MWh. The inputs come from published literature, case studies of other renewable energy technology development, and experience of the industrial partners within the DTOceanPlus consortium.

This work **highlights the need for a mix of policies to drive down the LCOE of ocean energy whilst minimising the overall investment needed for wide-scale deployment of these technologies**. This work also shows the wider benefits to society that can be achieved by ocean energy, and how these benefits can outweigh the costs involved. In all the scenarios discussed in this report, the **open-source design tools being developed in the DTOceanPlus project can contribute to the development of the ocean energy sector, facilitating both incremental and step-change cost reductions**.

Download the report `D8.3 Feasibility and cost-benefit analysis <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D8.3-Feasibility-and-cost-benefit-analysis>`_, 
and associated journal article `Implementing Radical Innovation in Renewable Energy Experience Curves <https://doi.org/10.3390/en14092364>`_.

D8.4. Developing Ocean Energy standards for Business management models in Ocean Energy
---------------------------------------------------------------------------------------

This report is the outcome of Task 8.4 “Specific sector standards for business management models for the ocean energy sector”, of the DTOceanPlus project. The task aims to define alternative business models for the ocean energy sector by developing a greater understanding of the **ocean energy sector's business models and recommending development routes to industrial roll-out** to improving the ocean energy sector's market opportunity.

The oceans represent the world's largest potential for renewable energy, with Europe at the forefront of ocean energy development, with wave and tidal energy representing the two most advanced technologies in the sector. Yet, tidal stream technologies are still at a pre-commercial stage and wave energy technologies, still at demonstration level. Thus, notwithstanding the significant progress of the sector in recent years, particularly in tidal stream, these technologies require further research, development, and innovation (RD&I) efforts to advance demonstration projects and partake in grid power's highly competitive markets. In addition, the high-up front costs and the embryonic stage of some ocean energy technologies make their development challenging.

Ocean energy in the present day has similar characteristics to the wind and solar sector of previous decades; as a developing technology, the LCOE is not cost-competitive with other alternatives for grid generation, making ocean energy a minority concern in the overall current generation mix. However, lessons can be learned from these sectors' trajectory to date, which has seen these technologies become cost-competitive and revolutionise many countries' generation mix.

The pathway to successful deployment required revenue support to bridge the initial gap to market; with costs falling through learning by doing, innovation, and economies of scale, as the market matures. Thus, market-led revenue support is key; however, targeted R&D support is required to assist with the journey from concept to commercialisation. Therefore, this work highlights **the need for alternative ocean energy applications as a good entry point into the market** and to undergo product development whilst generating revenue. This could allow for additional RD&I funds to be developed by initiating small-scale projects, thereby placing ocean energy in a better position to power the main grid when the need arises. In addition, synergies exist with other offshore sectors for ocean energy to provide localised power.

The task aimed to build on Task 8.3 and define a scenario for industrial roll-out analysis. Standard approaches to business models were developed by combining the value of the DTOceanPlus suite of tools with a deep knowledge of the potential markets that ocean energy technology can be applied to and the supply chain in place to exploit the opportunities. The report demonstrates how various stakeholders' application of the design tools can support the sustainable impact of potential markets upon the sector and its commercialisation prospects by developing alternative business models. The alternative business model approaches include pricing methods that can support business, funding and support cases.

Potential scenarios for industrial roll-out are presented, with a focus on four of the most detailed alternative markets identified within Deliverable D8.1, namely: isolated power systems (islands or microgrids), offshore oil and gas, offshore aquaculture, desalination & coastal resiliency. Business modelling canvasses were developed for each potential alternative market to create a more robust business proposition and identify barriers to market access that ocean technology developers can address. However, following stakeholder engagements and market testing, there was recognition of similarities that cut across various potential markets and that standard business models may need to be applied across these distinct market sections. Therefore, the approach taken was to **categorise these alternative markets into common themes that provide a clearer sense of progression for ocean generation technologies and insight into the shared technical considerations**. These markets were reframed to consider business propositions for **partial power supply for the whole system**, **primary power supply for subsystems**, and supply applicable to regions with limited power options for **resiliency markets for remote communities**. Therefore, common themes and potential routes to market that arose from these were balancing requirements with **hybrid systems**, **multipurpose solutions**, and **unique solutions for wave and tidal**.

The alternative markets explored within this report may act as supply chain accelerators for ocean energy if collaborative projects are undertaken within these areas. Aquaculture and offshore platforms have already been identified as contenders for these activities within deliverable 8.2, primarily because of their offshore location. Any identified collaborative areas could be worked into project proposals as an added benefit. The geographical spread of the markets was reviewed within this report, identifying potentially viable markets within Europe (aquaculture, oil and gas) and more prevalent ones elsewhere in the world (microgrids, desalination). This creates a discrepancy with manufacturing and component supplier location, which necessarily needs to be local (e.g., Europe-based). These alternative markets could provide an entry point to export markets.

When looking to access alternative markets and assess the suitability of business models, ocean energy developers could consider non-traditional procurement models to overcome potential barriers such as access to capital investment, technical and operational responsibilities. These procurement models could alleviate concerns and open up markets that may otherwise have been unwilling to change from standard diesel-based solutions.

The work also presents **a series of potential market blockers identified** that contribute to tidal and wave energy unable to access either mainstream grid or alternative markets; some **recommendations to help alleviate some of these blockers** are outlined.

The open-source design tools developed in the DTOceanPlus project can contribute to the development of the ocean energy sector. The Structured Innovation design tool can assist with facilitating ways to identify and overcome blockers; the Stage Gate tool can then be used to assess and guide the technology development; followed by the Deployment and Assessment tools to design optimised arrays, facilitating a wide-scale deployment of ocean energy technologies to generate electricity for these markets.

Download the report `D8.4 Developing Ocean Energy standards for Business management models in Ocean Energy <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D8.4-Developing-ocean-energy-standards-for-business-management-models-in-ocean-energy>`_.

D8.5. Analysis of the effect of the overall legal institutional and political frameworks
----------------------------------------------------------------------------------------

This report is the outcome of Task 8.5 'Analysis of the effect of the overall legal institutional and political frameworks' of the DTOceanPlus project. It provides a critical evaluation of the ocean energy sector's legal, institutional, and political frameworks with an identification and analysis of barriers and enabling factors for the deployment of ocean energy. The task focuses first on an initial review of the current political and regulatory frameworks on a set of countries to consolidate up to date information and set the basis for the identification of the main challenges faced by the sector. Subsequently, a critical analysis of the main barriers and enablers was carried out, supported by a questionnaire conducted to regulators, technology developers and test site managers. This survey aimed at gaining further insight into in-depth experiences on the subject. 

Ocean energy is bringing unique challenges to marine governance frameworks, with legal, institutional, and political issues being frequently perceived as significant non-technological barriers to the advancement of the sector. Based on the literature review and respondents' perceptions, several challenges and enabling features were identified within the national and international policies and the consenting procedures namely regarding the existent legislation, environmental impact assessment and monitoring, guidance, marine spatial planning, stakeholder consultation and entities involved in the process. Results from this task can provide guidance to future policy instruments and give support to consenting measures to be designed in a more informed and effective manner and to help accelerate the development of the sector.


Download the report `D8.5 Analysis of the effect of the overall legal institutional and political frameworks <https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D8.5-Relevant-legal-institutional-and-political-frameworks>`_, 
abd associated journal article `Legal and Political Barriers and Enablers to the Deployment of Marine Renewable Energy <https://doi.org/10.3390/en14164896>`_.

