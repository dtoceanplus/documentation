.. _dtop-explanation:

*******************************************
Background and theory
*******************************************

This section covers some background on the development of the DTOceanPlus suite of tools.

.. toctree::
    :maxdepth: 1

    dtop-use-cases
    dtoceanplus-development
    dtop-technologies-and-architecture



Ocean Energy Technologies considered
------------------------------------

DTOceanPlus is designed to consider those ocean energy technologies that capture energy either 
from the waves or from tidal currents (also known as tidal stream), using devices known as 
Wave and Tidal Energy Converters (WEC/TEC). 
These technologies may be either moored floating devices, or fixed to the seabed/coastline. 
This gives four types of technologies that will be assessed by DTOceanPlus:

1.	Fixed wave energy converter
2.	Floating wave energy converter
3.	Fixed tidal stream energy converter
4.	Floating tidal stream energy converter

DTOceanPlus is not designed to directly assess other ocean energy technologies, 
such as tidal range (impoundment via lagoons/barriers), offshore wind, 
ocean thermal energy conversion (OTEC), salinity gradients, etc.

Market Analysis reports
-----------------------

The DTOceanPlus project also produced a set of complimentary reports dealing with :ref:`market-analysis-reports` which are briefly summarised in the documentation.

1. Analysis of potential markets for Ocean Energy technology
2. Analysis of the European supply chain
3. Feasibility and cost-benefit analysis
4. Developing specific sector standards for business management models
5. Analysis of the effect of the overall legal institutional and political frameworks

.. toctree::
   :maxdepth: 1
   :hidden:

   market-analysis-reports

