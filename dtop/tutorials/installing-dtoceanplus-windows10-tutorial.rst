
.. _installing-dtoceanplus-windows10-tutorial:

Installing DTOceanPlus on Windows 10
=====================================

.. note:: These are the current instructions for installing the development version of the tools. 

For additional details on the installation process including screenshots, how the various scripts work, and how to configure a company intranet installation, please see the `documentation within the installation repository <https://gitlab.com/dtoceanplus/dtop_inst/-/blob/master/README.md>`_.

Pre-requisites
--------------

Installation of DTOceanPlus currently requires a computer with at least the following specifications:

- Memory (RAM): 12 GB minimum, 16 GB+ recommended
- Processors (CPUs): 2 minimum, 4+ recommended
- Disk Space: >10 GB free space
- Windows 10 Pro, Enterprise, or Education (not Windows 10 Home)


Download and install Docker Desktop from https://docs.docker.com/docker-for-windows/install/, following the instructions on the Docker website. 
   
.. Note:: Hyper-V should be enabled on Windows

          This may require administrator rights, and changing some system settings to enable virtualisation.
          See (https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v) for details.

In Docker Desktop, click the settings (gear icon) and check the following:

   a. Use the WSL-2 based engine is unticked (this then uses Hyper-V)
   b. Under Resources>advanced the Memory is set to 8GB (or more), but not allocating all resource to Docker
   c. Under Resources>file sharing, add the location you will install the DTOceanPlus tools (see step 2 under installation)

Installation
------------

1.  Download the installer files and Site Characterisation databases:

    a. From https://gitlab.com/groups/dtoceanplus/-/packages click on the latest version of ``dtop_win_inst`` and download the ``dtop_win_inst_1.x.x.zip`` file.

    b. From https://gitlab.com/dtoceanplus/dtop_sc_databases download the ``dtop_inst_1.5_sc_databases.tar.gz`` archive (or newer version if available).

2.  Extract the installation files from the ``dtop_win_inst_1.x.x.zip`` archive to a convenient location on your local hard disk. Note that this cannot contain any spaces (or special characters) in the file path, and this folder needs to remain on your computer to use the tools. 

3.  Move the Site Characterisation databases ``tar.gz`` archive into this folder, but you do not need to extract it.

    .. Important:: Docker Desktop should be running before installing the tools.

4.  Run the ``dtop_win_inst.exe`` programme, which *will need authorisation/admin rights* on your computer. 
    You may recieve a warning "Windows protected your PC" about an unrecognised app, click ``More info`` and ``Run anyway``.
    This will then guide you through the 5-step installation process:

    1. The first screen checks the configuration set in the ``dtop_inst.env`` file and the settings for Docker desktop. 
       
       - Click the ``Check for Updates`` button to check if updated versions of any tools have been released. A dialog shows the applicable installed and updated version numbers.    Click ``Yes`` to update the ``dtop_inst.env`` file to use these versions in the installation, or ``No`` to install previously configured versions.
       - Click ``Continue`` to display the licence, then ``Proceed to Confirm``. 
       - Confirm you accept the licence on the next screen by clicking the ``Accept`` button.

    2. **Select modules**  In step 2, you can select which modules to install. 
   
       .. Note::  The **Main Module** is required to manage Projects and Studies in all modules.
                  
                  The **Catalogues** module is required to use the following Deployment Design tools: Energy Transformation, Energy Delivery, Station Keeping, and Logistics and Marine Operations. 

    3. **Download modules from GitLab** Each selected modules will then be downloaded in turn. Each module will show a progress bar. *This step may take some time*, depending on internet download speed and number of modules to be installed. Once complete, a list of all the module Docker Images downloaded from GitLab is shown. Click ``Continue`` to proceed. 

    4. **Module deployment** The modules are then deployed in turn. This step will show a short progress bar for each module, then a list of all the module Docker Containers deployed is shown. Click ``Continue`` to proceed.

    5. **Finish installation**   If there are no any unexpected issues during the previous steps, the final screen shows how to open the modules, or see section Running the modules below. Click the ``Launch DTO+`` button to open a new browser window with the main module login page. 

At this point, the :ref:`dtop-getting-started` tutorial will guide you through the process of loging in, setting up users, 
and creating projects and studies. The following steps are optional, for more advanced use.


Optional Post-installation steps
--------------------------------

.. Note:: Portainer can optionally be used to monitor the performance and manage the Docker containers for all modules. See :ref:`dtop-troubleshooting-portainer`.


Running the modules individually
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Docker Desktop should be started and running before using the tools. It may take a few minutes before all modules are available, so if you get an error please retry after a minute or so. See also :ref:`dtop-troubleshooting`.

When installed on your computer, open http://mm.dtop.localhost in a web browser to access the tools.
See :ref:`dtop-getting-started` for more details.

Each of the modules can also be accessed directly in standalone mode at the following addresses if the Main Module is not installed. 

- Structured Innovation (SI) http://si.dtop.localhost
- Stage Gate (SG) http://sg.dtop.localhost
- Deployment Design tools:
  
  - Site Characterisation (SC)  http://sc.dtop.localhost
  - Machine Characterisation (MC) http://mc.dtop.localhost
  - Energy Capture (EC) http://ec.dtop.localhost
  - Energy Transformation (ET) http://et.dtop.localhost
  - Energy Delivery (ED) http://ed.dtop.localhost
  - Station Keeping (SK) http://sk.dtop.localhost
  - Logistics and Marine Operations (LMO) http://lmo.dtop.localhost
  
- Assessment Design tools:

  - System  Performance and Energy Yield (SPEY) http://spey.dtop.localhost
  - System Lifetime Costs (SLC) http://slc.dtop.localhost
  - System Reliability, Availability, Maintainability, Survivability (RAMS) http://rams.dtop.localhost
  - Environmental and Social Acceptance (ESA) http://esa.dtop.localhost

- Catalogue Module (CM)  http://cm.dtop.localhost

.. Note:: If the DTOceanPlus tools are installed on a company server and accessed via the company intranet 
          the ``dtop.localhost`` part of these addresses will be different, but your local IT will advise. 

The DTOceanPlus Monitor application can also be used to assist with troubleshooting, as described in :ref:`dtop_mon`.


Uninstalling the DTOceanPlus tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. To uninstall the DTOceanPlus tools, run the ``dtop_win_inst.exe`` programme again. 
#. Click the ``Uninstall`` button to start the uninstallation process. 
#. Select the module(s) to uninstall, then ``Continue`` to remove these. 

Updating DTOceanPlus modules to the latest versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Within the ``dtop_win_inst.exe`` programme, you can click the ``Check for Updates`` button, which compares the list of modules to be installed with the latest list on the GitLab repository. 
#. This shows a list of differences (if any), click ``Yes`` to update the local list of versions to be installed.

   .. Tip:: You can also manually edit the ``dtop_inst.env`` file to update the module version tags, for example if testing a development version. Then just run the  ``dtop_win_inst.exe`` programme as normal.

#. Before updating, it is best to remove the old versions (but this is not required).

   a. Click the ``Uninstall`` button to start the uninstallation process. 
   b. Select the module(s) to uninstall, then ``Continue`` to remove these. 

#. Click ``Continue`` in the main window to run through the installation process as described above.
   
   - In step 2, the updated modules will be pre-selected 

.. important:: Make sure to do a full refresh of your browser when loading each module for the first time after updating.

               Press ``Ctrl+F5`` keys or hold ``Ctrl`` and click refresh. See the browser's help for details.

Redeploying DTOceanPlus modules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Add in when/why need to do this

If you need to redeploy any of the DTOceanPlus modules, run the ``dtop_win_inst.exe`` programme.
Click the ``Redeploy`` button to start the redeployment process. 
Select the module(s) to uninstall, then ``Continue`` to redeploy these. 
