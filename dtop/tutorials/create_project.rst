.. _mm-tutorial-create_project: 

************************
Create Project & Studies
************************

This tutorial will explain how to create a project and studies. 

See :ref:`glossary` for the definition of the main concepts.

Create the project
------------------

#. Connect to the DTOceanPlus application with a user with *User* profile.

#. On the Dashboard click the ``Projects`` button.

   .. image:: ../images/Dashboard.jpg
     :alt: Dashboard
     :scale: 50%
     :align: center

#. The *Projects* page lists the existing project. Click the ``Create New Project`` button to create a new project.

#. Fill the information of the project: project name, Technology, Type of Device.
   You can also add a logo and a short desription.

   .. image:: ../images/CreateProject.jpg
     :alt: Dashboard
     :scale: 50%
     :align: center

#. You can edit the project information using the ``Edit`` button in the projects list. 

#. Click the ``View`` Button in the list to open the project.


Create studies
--------------

A project is composed of one or several studies. 

.. note::
   It is possible to create different kind of studies:
   
   - *Standalone*: with only one module.
   - *Design and Assessment*: with all the modules.

   By default a Project will open the *Design and Assement* tab to use the integrated suite of tools. 


To create a new Design and Assement study using the integrated suite of tools
==============================================================================

On the *Design and Assement* tab:

#. First Select an existing or create a new *Site* and *Machine* 

   a. Click ``Select`` to open a list of exiting Sites. Click ``Select`` next to the corresponding site to use that one. Clicking ``Open`` will open the :ref:`Site Characterisation <sc-home>` module in a new browser tab.

   b. Alternatively, click ``Create``, then enter a name and select the required complexity level.  Clicking ``Open`` will open the :ref:`Site Characterisation <sc-home>` module in a new browser tab.

   c. Similarly, click ``Select`` or ``Create`` for Machine Characterisation, to define the machine to be used in the study. Note that the complexity leve for MC and Energy Capture must be the same.

#. Click the ``Create New Study`` button. Fill in the study name, optionally enter a description, and click ``Create``. 
#. Click ``Open`` to show the list of modules. You can also ``Edit`` the Name/description, or ``Delete`` the Study.
#. Run through the Deployment and Assement modules in order to build up your study.

   a. Click ``Create`` next to :ref:`Energy Capture <ec-home>` to create a new EC Entity, and fill in the details. Note that the complexity for EC depends on the MC complexity, and the Machine type is determined at Project level.  
   b. Click ``Open`` to open the Energy Capture module in a new browser tab, and follow the instructions in the :ref:`Energy Capture tutorials <ec-tutorials>` to determine the energy captured by the device or array. 
   c. Once this is complete, return to the Main Module page.

#. Repeat this process for each of the modules you wish to use in turn.

To create a new Standalone study using one of more tools independently
==============================================================================

On the *Standalone* tab:

#. Click ``Create New Standalone Study`` button.
#. Fill in the Study Name, select the Study Module, and optionally enter a description.
#. Click ``Open`` to show this study
#. Click ``Create`` to register a new Entity for the selected module. Fill in the Name, optionally add a description, and select the required complexity level.
#. Click ``Open`` to load the module in a new browser tab. 

.. Note:: 

   In standalone mode, the user has to provide all of the input data which would come from previous modules in integrated mode. 

   This is therefore a more advanced use case.

Share the Project
-----------------

You can share the project with other users:

#. Go to the *Users* tab.

#. Register new users to your project.

.. warning::
   * The current version of the project does not check concurent access to the project.
   * All users have the same rights and can unregister any other users.
   * Users linked to a project cannot be deleted.

