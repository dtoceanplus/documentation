.. _dtop-assessment-tools-tutorial:

Use the Assessment Design tools to assess device/array metrics 
====================================================================

This tutorial outlines the steps needed to assess metrics of an array, device, or subsystem for a project/deployment. 
This uses the outputs of the Deployment Design tools. 
It only covers the steps at a high-level, further details of the specific steps required in each tool is provided within the documentation of that tool, as linked below.    

.. figure:: ../figures/assessment-tools.svg
    :align: center

    Assessment Design tools 

1.  Use the :ref:`Deployment Design tools <dtop-deployment-tools-tutorial>` 
    to design an optimal deployment for a device or array of devices. This gives the array layout and configuration, design of subsystems, a bill of materials, and the hierarchy of how these are connected.     

2.  (optionally) In the :ref:`System Performance and Energy Yield (SPEY) <spey-home>` tool,
    evaluate projects in terms of energy performance at array, device, or subsystem level.

    a. Compute dimensionless parameters (**Efficiency**) and dimensional parameters (**Alternative Metrics**), given the technical design of the ocean energy plant and the power production of the different subsystems, at different level of aggregation (array and device level) and facilitate the visualisation of these outputs to the user.
    b. Estimate the **Energy Production** at different level of aggregation (array and device level) accounting for the probabilistic distribution of the downtime throughout the life of the project, within different timescales (lifetime of the plant, annual and monthly energy production) and facilitate the visualisation of these outputs to the user.
    c. Show results in terms of **Power Quality** (Reactive vs Active power to the grid and as outputs per device) obtained from the deployment tools.
  
3.  (optionally) In the :ref:`System Lifetime Costs (SLC) <slc-home>` tool,
    evaluate projects from the economic perspective.

    a. Compile a **Bill of Materials**, i.e. an inventory of materials, assemblies, and components, including the quantities of each, as well as the installation operations required to construct a given ocean energy farm
    b. Perform a **techno-economic assessment**, estimating the LCOE of the farm, or using other alternative metrics for early-stage technologies.
    c. Perform a **Financial Assessment**, to evaluate the financial attractiveness of the project from the perspective of the investor, assessing project profitability.
    d. Undertake a **Benchmark Analysis**, to compare the economic and financial results of the project against reference values from wave and tidal projects

    All assessments produced by the System Lifetime Costs module are carried out based on the design outputs of the Deployment design tools but also project characteristics introduced by the user and a catalogue of reference cost-breakdowns of ocean energy projects at different development stages.
  
  
4.  (optionally) In the :ref:`System Reliability, Availability, Maintainability, Survivability (RAMS) <rams-home>` tool,
    evaluate the reliability aspects of a marine renewable energy project, in terms of the following metrics:
    
    a. **Reliability** - the ability of a structure or structural member to fulfil the specified requirements, during the working life, for which it has been designed.
    b. **Availability** - the probability that a system or component is performing its required function at a given point in time or over a stated period of time when operated and maintained in a prescribed manner. In engineering applications, the availability of a device is the ratio of the uptime to the sum of uptime and downtime during the design lifetime. The availability of the array is the arithmetic average of that of all devices in the array.
    c. **Maintainability** - the ability of a system to be repaired and restored to service when maintenance is conducted by personnel using specified skill levels and prescribed procedures and resources.
    d. **Survivability** - the probability that the critical structural and mechanical components can survive the ultimate and fatigue loads during the design lifetime.
  

5.  (optionally) In the :ref:`Environmental and Social Acceptance (ESA) <esa-home>` tool,
    evaluate the environmental and social impacts of a given wave and tidal energy projects.

    Following the regulatory context of environmental risk assessment and socio-economic opportunities, the Environmental and Social Acceptance (ESA) module was structured into four different parts representing four complementary assessments:

    a.  Identification of the potential presence of endangered species in the area of MRE installation (i.e. species included in the IUCN red list);
    b.  Environmental impact assessment estimated for the main environmental stressors pre-identified as potential stressors from MRE such as the underwater noise or the collision risk between vessels/devices and the marine wildlife;
    c.  Estimation of the carbon footprint and the green-house effect of the project in terms of Global Warming Potential (GWP) and Cumulative Energy Demand (CED); and
    d.  Information to improve the social acceptance of the project considering socio-economic opportunities.

.. note:: To be further developed, reviewing high level steps for each module 
    (particularly integrated mode operations).
