.. _dtop-structured-innovation-tools-tutorial:

***********************************
Use the Structured Innovation Tools 
***********************************

.. warning:: Draft to be reviewed and updated

This tutorial outlines the steps to use the integrated QFD/TRIZ process 
and the FMEA method to guide innovation using the Structured Innovation tools. 
It only covers the steps at a high-level, further details of the specific steps 
required are given in the :ref:`Structured Innovation tool <si-home>` documentation.


#.  After creating a new QFD/TRIZ study, start by defining the project’s top-level 
    objective that will be the basis of the QFD/TRIZ study. This is also where the 
    user defines the list of the customer needs (the WHATs) broadly. In the context 
    of developing a new product, this is a list of customer requirements. These 
    requirements (often general, vague, and difficult to implement directly) are 
    prioritised in order of importance.

#.  Next, scan the design space. 
    Define the measurable functional requirements (the HOWs) to satisfy the 
    customer requirements (the WHATs) and how much each functional requirement 
    impacts each customer requirement. 

    a. Define the functional requirements, with target value, direction of 
       improvement, and level of difficulties.
    b. Define the level of impact between each customer and functional requirement.
    c. Define the correlations between the functional requirements.

#.  Then, identify attractive areas of innovation and assess contradictions.
    Inventive inspiration is provided for the user using the TRIZ contradictions 
    matrix.
    In addition, the TRIZ methodology can ensure completeness in the key parameters 
    that define the design space using provocative prompts to provide the well-known 
    forty inventive principles and other tools to solve contradictions within the QFD.

    a. Enter TRIZ Classes
    b. Document Existing Solutions
    c. Collate and Interpret Results

#.  Finally, assess technical risk. These are framed using the ‘concept’ or ‘design’ 
    FMEA component. The component provides ratings for each defect or failure in 
    terms of severity, occurrence, and detection.

    a. Define the Action Level and Occurrence Limit
    b. Define the Design Requirements
    c. Define the Failure Modes for each Design Requirement.
    d. Define the Failure Mode Effects
    e. Define the Causes and Likelihood of each Failure Mode.
    f. Define the Design Controls
    g. Define Additional Mitigations

.. note:: TODO: Add next steps?