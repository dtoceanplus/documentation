.. _mm-tutorial-compare: 

Fork & Compare Studies
======================

After having designed and assessed a project, the user can *fork* a study
to try a slightly different design and check if this improve the overall results.

For example, a user may wish to test a different network configuration in Energy Delivery including the cable installation activities in the Logistics and Marine Operations tool. 
They would then fork after Energy Transformation, and create new entities in subsequent modules, as shown below.

.. image:: ../figures/forking.svg


Fork a study
------------

#. After creating a study and running some modules, go to the Studies tab of the project.

#. Click the ``Fork Study`` button.

#. In the dialog enter the new study name and select the module where you want to fork after.

   .. image:: ../images/Fork_Study.jpg
      :alt: Fork Study
      :align: center
      :scale: 80%

   .. tip:: You can use the *Description* field to note what kind of changes you
     plan to do with this fork. 

#. After validating a new study is created.

   .. warning:: All the modules **before and including** the fork are **common to both studies**.
      Therefore any change made to these modules will affect both studies.



Compare studies
---------------

**NOT Implemented yet**
