.. _mm-tutorial-users: 

Manage users
============

This tutorial describes how to create and manage users for the DTOceanPlus.

Roles
-----

There are two different roles in the DTOceanPlus application: Administrator and User.

- An **Administrator** can create and manage users, but cannot create or manage projects.
- A **User** can only create and manage projects.

.. note::
   The default user of the DTOceanPlus suite is named *admin@dtop.com* with default password *adminadmin*.

Create a user
-------------

#. Connect to the application with an *administrator* user.

#. On the Dashboard click the ``Users`` button.

#. Click the ``Create New User`` button.

#. Fill the form with user name, email, password, and select a role for the user.

   .. image:: ../images/CreateUser.jpg
     :alt: Create User Form
     :scale: 50%
     :align: center


Edit a user information
-----------------------

To edit the user information click the ``Edit Info`` button for the user in the list.

.. image:: ../images/EditUser.jpg
   :alt: Edit user
   :scale: 50%
   :align: center

Change password
---------------

The change the password of a user, the administrator needs to use the *Edit Password* form,
which is accessible from the ``Edit password`` button in the list.

.. image:: ../images/EditPassword.jpg
   :alt: Edit user
   :scale: 50%
   :align: center

Delete a user
-------------

To delete a user click the corresponding ``Delete`` button in the list.

.. note::
   It is not possible to delete a user linked to existing projects.
