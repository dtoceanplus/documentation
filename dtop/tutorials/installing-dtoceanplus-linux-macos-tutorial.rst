
.. _installing-dtoceanplus-linux-macos-tutorial:

Installing DTOceanPlus on Linux or macOS
========================================

.. warning:: These are a draft to be reviewed and updated. 


For additional details on the installation process including screenshots, how the various scripts work, and how to configure a company intranet installation, please see the `documentation within the installation repository <https://gitlab.com/dtoceanplus/dtop_inst/-/blob/master/README.md>`_.

Pre-requisites
--------------

Installation of DTOceanPlus currently requires a computer with at least the following specifications:

- Memory (RAM): 12 GB minimum, 16 GB+ recommended
- Processors (CPUs): 2 minimum, 4+ recommended
- Disk Space: >10 GB free space


Download and install Docker, as per the instructions.
   
- For macOS, download Docker Desktop from https://docs.docker.com/docker-for-mac/install/. 

  - In Docker Desktop, click the settings (gear icon) and check the following:
  
    a. Under Resources>advanced the Memory is set to 8GB (or more)
    b. Under Resources>file sharing, add the location you will install the DTOceanPlus tools (see step 2 under installation)

- For Linux, download Docker Engine from https://docs.docker.com/engine/install/.
      


Installation
------------

1.  Download the installer files and Site Characterisation databases: from https://gitlab.com/dtoceanplus/dtop_inst/. Only the files ``dtop_inst_1.0.0_linux_macos.tar.gz`` and ``dtop_inst_1.5_sc_databases.tar.gz`` are needed (or newer versions if available).

    a. From https://gitlab.com/groups/dtoceanplus/-/packages click on the latest version of ``dtop_inst_linux`` and download the ``dtop_inst_linux_1.x.x.tar.gz`` file.

    b. From https://gitlab.com/dtoceanplus/dtop_sc_databases download the ``dtop_inst_1.5_sc_databases.tar.gz`` archive (or newer version if available).

2.  Extract the installation files from the archive to a convenient location on your local hard disk. Note that this cannot contain any spaces in the file path, and this folder needs to remain on your computer to use the tools.  
3.  Move the Site Characterisation databases archive into this folder, but you do not need to extract it.

    .. Important:: Docker Desktop should be running before installing the tools.

4.  In a bash terminal (macOS Terminal) change directory to the DTOP installation one, for example ``cd ./dtop_inst_1.0.0`` ``↵``, and 
    check to make sure that the scripts are executable: ``chmod u+x *.sh`` ``↵``.

5.  Run the  installation script ``./dtop_inst.sh`` ``↵``.

    1. **Licence** The first screen displays the licence. Use up/down arrows or PgUp/PgDn to scroll, press Tab to highlight ``Proceed to confirm`` then press Return. Confirm you accept the licence on the next screen by using Tab and Return.

    2. **Select modules**  In step 2, you can select which modules to install. *All option is selected by default*. Use up/down arrows and spacebar to change options, then Enter to confirm.

       .. Note::  The **Main Module** is required to manage Projects and Studies in all modules.
                    
                  The **Catalogues** module is required to use the following Deployment Design tools: 
                  Energy Transformation, Energy Delivery, Station Keeping, and Logistics and Marine Operations. 

    3. **Download modules from GitLab** Each selected modules will then be downloaded in turn. Each module will show a progress bar, and will show a black command screen between modules. *This step may take some time* depending on internet download speed and number of modules to be installed). The last page of this Step presents a list of all the module Docker Images downloaded from GitLab. Select *Continue* to proceed. 

    4. **Module deployment** The modules are then deployed in turn. This step will show a black command window between each module, with a short progress bar for each module. The last page of this Step presents a list of all the module Docker Containers deployed. Select *Continue* to proceed.

    5. **Finish installation**   If there are no any unexpected issues during the previous steps, the final screen shows how to open the modules, or see section Running the modules below. You can now close this command window.

At this point, the :ref:`dtop-getting-started` tutorial will guide you through the process of loging in, setting up users, 
and creating projects and studies. The following steps are optional, for more advanced use.


Optional Post-installation steps
--------------------------------

.. Note:: Portainer can optionally be used to monitor the performance and manage the Docker containers for all modules. See :ref:`dtop-troubleshooting-portainer`.


Running the modules individually
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Docker Desktop should be started and running before using the tools. It may take a few minutes before all modules are available, so if you get an error please retry after a minute or so.

When installed on your computer, open http://mm.dtop.localhost in a web browser to access the tools.

Each of the modules can also be accessed directly in standalone mode at the following addresses. This is also how to access them if the Main Module is not installed. 

- Structured Innovation (SI) http://si.dtop.localhost
- Stage Gate (SG) http://sg.dtop.localhost
- Deployment Design tools:
  
  - Site Characterisation (SC)  http://sc.dtop.localhost
  - Machine Characterisation (MC) http://mc.dtop.localhost
  - Energy Capture (EC) http://ec.dtop.localhost
  - Energy Transformation (ET) http://et.dtop.localhost
  - Energy Delivery (ED) http://ed.dtop.localhost
  - Station Keeping (SK) http://sk.dtop.localhost
  - Logistics and Marine Operations (LMO) http://lmo.dtop.localhost
  
- Assessment Design tools:

  - System  Performance and Energy Yield (SPEY) http://spey.dtop.localhost
  - System Lifetime Costs (SLC) http://slc.dtop.localhost
  - System Reliability, Availability, Maintainability, Survivability (RAMS) http://rams.dtop.localhost
  - Environmental and Social Acceptance (ESA) http://esa.dtop.localhost

- Catalogue Module (CM)  http://cm.dtop.localhost

Note that if the DTOceanPlus tools are installed on a company server and accessed via the company intranet the ``dtop.localhost`` part of these addresses will be different, but your local IT will advise. 


Uninstalling the DTOceanPlus tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The uninstallatiom process for the DTOceanPlus tools is as follows:

1.  In a bash terminal run the ``./dtop_uninst.sh`` script to start the uninstallation script. 
  
2.  At the end, you will be asked if you want to remove all containers with a y/N option. 
    Type ``Y↵`` to delete the files from your computer, or ``N↵`` to retain these files for re-installation.

Updating DTOceanPlus modules to the latest versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. Note:: To be written.
