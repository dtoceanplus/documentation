.. _dtop-deployment-tools-tutorial:

Use the Deployment Design tools to design an optimal array layout
====================================================================

This tutorial outlines the steps needed to design an optimal array layout using the Deployment Design tools in sequential order. 
It only covers the steps at a high level, further details of the specific steps required in each tool is provided within the documentation of that tool, as linked below.    


Before outlining the steps taken in each tool, it is important to note that unlike the other tools within the suite, the Deployment Design tools should be run in sequential order.

.. figure:: ../figures/deployment-design-tools.svg
    :align: center

    Deployment Design tools run in sequence


#. Start with the :ref:`Site Characterisation (SC) <sc-home>` tool, to select/enter details about the site including the resource available.

   a. Select or import databases:

      1. At low complexity, select bathymetry and 1D resource data for a reference site.
      2. At medium complexity, select bathymetry and 2D resource data for a reference site.
      3. At high complexity, enter bathymetry and 1D/2D resource data for a custom site. 

   b. Extract variables from the database and compute statistics.
   c. See a graphical overview of results with site information then provided to other tools.

#. In the :ref:`Machine Characterisation (MC)<mc-home>` tool,
   enter properties of the wave or tidal energy converter. 

   a. Input general parameters: unit cost, power rating, material, plus cable and seabed connection types
   b. Input dimensions: length, width, height, characteristic dimension, wet & dry areas, submerged volume, etc.
   c. Model the efficiency, limits, and number of generators. 
      For high complexity level for WECs, the tool also calculates hydrodynamic coefficients. 

#. In the :ref:`Energy Capture (EC) <ec-home>` tool, 
   calculate the hydrodynamic energy captured by the device including array interactions in the module. 

   a. Input the farm [, site, and machine data] UPDATE
   b. The tool can be used in two design modes:
      
      1. Estimate the array performance based on a given array layout.
      2. Find the array layout that maximises the energy production of the array.
         
   c. View results of the farm layout, farm & device energy production & array efficiency

#. In the :ref:`Energy Transformation (ET) <et-home>` tool,
   design the different energy transformation steps:

   1. Hydrodynamic to Mechanical (Mechanical Transformation);
   2. Mechanical to Electrical (Electrical Transformation) and Control;
   3. Electrical to Grid (Grid Conditioning).

   by:

   a. Select configuration parameters/design options
   b. Perform assessment of the design options
   c. View results at PTO/device/array level, in terms of performance, reliability, cost, mass, and bill of materials of the three energy transformation steps.   

#. In the :ref:`Energy Delivery (ED) <ed-home>` tool,
   design the electrical infrastructure to transmit power to shore.
   
   a. Enter site, device, array data and then select configuration options for the electrical infrastructure, including one of six network topologies (e.g. direct, radial, star), plus cable installation and protection options.
   b. Run the design algorthim to calculate a network layout.
      Components such as cables and connectors are selected from the electrical network equipment catalogue. 
   c. Review optputs of the network design option(s): the energy/power delivered to shore and network losses, the total cost and bill of materials for the electrical components used, plus a hierarchy of how they are connected.
   d. Select the design option to use.

#. In the :ref:`Station Keeping (SK) <sk-home>` tool, 
   design mooring and foundation subsytems to keep the device on station. This includes mooring lines for floating structure, anchors, and foundation for fixed structures.

   a. Input mooring system and foundation properties
   b. Run analysis: 

      1. Optional design of catenary mooring system, 
      2. ULS & FLS analyses of mooring systems,
      3. Optional design of foundations and anchors, 
      4. ULS analyses of foundations and anchors.

   c. View result of mooring system and foundation/anchor design and assessment, including the total cost and bill of materials for the components used, a hierarchy of how they are connected.

#. In the :ref:`Logistic and Marine Operations (LMO) <lmo-home>` tool,
   design solutions of logistical infrastructure and marine operations scheduling.
 
   a. Input farm and device characteristics, sub-system characteristics, site data, simulation statistics, and identify phases to run [UPDATE]
   b. Specify lifecycle phase requirements and operation preferences and methods
   c. Carry out design of the selected project lifecycle phases (Installation, O&M, Decommissioning). The logistical solutions include: 

      1. Infrastructure solutions – optimal selection of vessels, ports and support equipment to carry out the installation/O&M/decommissioning operations
      2. Operation plans – operation durations, weather contingencies, start dates, end dates.
      3. Operation costs – cost of operations, including vessel chartering costs, fuel costs, port costs and equipment costs. These costs grouped into installation, maintenance and decommissioning

   d. View results, including operation durations, infrastructure selection and total costs,
      for the desired project lifecycle phases (Installation, O&M, Decommissioning).

Assessment of the overall design can then be conducted with the :ref:`Assessment tools <dtop-assessment-tools-tutorial>`. 

.. note:: To be further developed, reviewing high level steps for each module 
    (particularly integrated mode operations).
