.. _dtop-stage-gate-tool-tutorial:

*********************************************************
Use the Stage Gate Tool to assess technology development
*********************************************************

.. warning:: Draft to be reviewed and updated

This tutorial outlines the steps to assess technology development using the Stage 
Gate tool. It only covers the steps at a high-level, further details of the specific 
steps required are given in the :ref:`Stage Gate tool <sg-home>` documentation.

The Stage Gate design tool is a framework that guides a user through a
technology assessment process. It is intended to be used by a wide variety of
stakeholders, including:

- Technology developers in the evaluation of their own technology and
- Investors and public funders to aid design of funding programmes and decision 
  making on candidate technologies

The main uses of the tool can be summarised as:

1.  An *Activity Checklist* that helps the user identify the stage that they have 
    reached in the technology development process. This presents the set of 
    activities that need to be completed at each Stage in the form of a checklist. 

2.  A *Stage Gate Assessment* which can be run in two complimentary modes:

    a. *Applicant mode*, where a technology developer answer questions about the 
       research and development activities they have performed for their technology.
       This includes both qualitative descriptons and quantitative metrics. 
       After completing the assessment it is possible to view the results, including
       deviation of metrics from target.

    b. *Assessor Mode* simulates how a funding body or technology development programme
       would take the application form submitted by a technology developer 
       (in this case submitted using the Applicant Mode) and evaluate the responses,
       metric results and justifications provided. 

3.  Identifying *improvement areas* for a device or technology. 
    These areas are derived from three sources:

    a. Where an insufficient number of activities have been completed in the 
       activity checklist.
       
    b. Where metrics have not met the relevant thresholds.

    c. Where assessor scores have not met the relevant thresholds.