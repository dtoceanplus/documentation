.. _dtop-getting-started:

********************************
Getting started with DTOceanPlus
********************************


This tutorial briefly outlines the steps needed to get started with the DTOceanPlus suite of tools. 
Further details are given in the following tutorials, so it is worth reading them before begining.

Logging in and setting up users
-------------------------------

#. Open the DTOceanPlus Main Module by opening http://mm.dtop.localhost in your web browser.
 
   - Note, this will be a slightly different address for a server installation.

#. The first step is to log in as an admin user, and create one or more *user* role accounts that can create and manage projects and studies within the tools.
   
   - This is covered along with login details in :ref:`mm-tutorial-users`.

#. To log out, click the button in the user menu at the top-right corner.
#. Log in with your new *user* role account credentials to start with projects and studies.

Create a project and study
--------------------------

#. On the *Projects* page, click the ``Create a new project`` button, and fill in the details including Name and Technology Type to start a new project
#. Click ``View`` to open this. By default it will open the *Design and Assement* tab to use the integrated suite of tools. 
#. Create a new or select an existing *Site* where the technology will be deployed. This uses the :ref:`Site Characteriation <sc-home>` tool.
#. Create a new or select an existing *Machine* which defines the key parameters of the technology to be deployed at the site. This uses the :ref:`Machine Characteriation <mc-home>` tool.
#. Click the ``Create a New Design & Assement Study`` button, and fill in the details. 
#. Click ``open``, then starting from *Energy Capture* click the ``Create`` buttons for modules you wish to use. Once you have created an *entity*, clicking the  ``open`` button wil load that module in a new browser tab.

For more details see the :ref:`mm-tutorial-create_project` tutorial.

