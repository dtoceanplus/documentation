.. _dtop-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through the key functionalities of the overall DTOceanPlus suite of tools.
They are intended for those who are new to DTOceanPlus.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

Showcasing features of the DTOceanPlus suite of tools
-----------------------------------------------------

As described in :ref:`dtop-use-cases`, there are different ways to use the suite of tools, 
optionally using the Deployment and Assessment tools, the Stage Gate tool, or the 
Structured Innovation tool in different order/combinations. 
The first four tutorials guide you through use of the tools at a high level, 
showcasing the main features.

#. :ref:`dtop-deployment-tools-tutorial`
#. :ref:`dtop-assessment-tools-tutorial`
#. :ref:`dtop-stage-gate-tool-tutorial`
#. :ref:`dtop-structured-innovation-tools-tutorial`


Installing the DTOceanPlus suite of tools 
---------------------------------------------------

DTOceanPlus is accessed via web-browser. It may either be installed on a company intranet server or installed locally on your computer (with a slightly different process per OS). 
If this has already been set up by your IT administrator, the installation tutorials can be skipped.
  
- :ref:`installing-dtoceanplus-windows10-tutorial`
- :ref:`installing-dtoceanplus-linux-macos-tutorial`

For installation on a company intranet server, please see the 
`README in the installation repository <https://gitlab.com/dtoceanplus/dtop_inst/-/blob/master/README.md>`_

Using the DTOceanPlus suite of tools 
------------------------------------

- :ref:`dtop-getting-started`. This tutorial outlines the first steps for using DTOceanPlus, with more detail in the following tutorials.

The :ref:`dtop-troubleshooting` guide may also be helpful when following these.

Main Module
===========

The Main Module (MM) is the main entry point of the DTOceanPlus suite. 
From this module the user can create :ref:`Projects <glossary-project>` and :ref:`Studies <glossary-study>`.

#. :ref:`mm-tutorial-users`  
#. :ref:`mm-tutorial-create_project`
#. :ref:`mm-tutorial-compare`
#. :ref:`mm-tutorial-dr`

Catalog Module
==============
The Catalog module (CM) is a special module which manages generic list of values
like electrical components, mooring lines, anchors, vessels...

These values will be used in the frame of the Design modules to define the project
with real characteristics, and by the Assessment modules to compute metrics.

The Catalog module allows the user to browse the values to check the full characteristics
of each item, and also to extend each catalogs by creating, editing and deleting values.

#. :ref:`cm-browse-catalogs`
#. :ref:`cm-edit-catalogs`


Other modules
=============

Tutorials on how to use the other tools can be found in the relevant section of the documentation. 


.. toctree::
   :maxdepth: 1
   :hidden:

   deployment-tools-tutorial
   assessment-tools-tutorial
   stage-gate-tool-tutorial
   structured-innovation-tools-tutorial

   installing-dtoceanplus-windows10-tutorial
   installing-dtoceanplus-linux-macos-tutorial
   getting-started

   manage_users
   create_project
   compare_studies
   digital_representation
   
   browse_catalogs
   edit_catalogs
