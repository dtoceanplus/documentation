.. _dtop-reference:

*************
API Reference
*************

This section of the documentation is the technical reference manual for the overall DTOceanPlus suite of tools.

This will cover the Main Module that manages the underlying data between the modules of the suite and studies.

It is formed of two components.

1. The OpenAPI specification for the Main Module and Catalgues Module. 
   This documentation, powered by `ReDoc <https://redoc.ly/redoc/>`_, describes the *implementation layer* of the Main Module and Catalgues Module, including the endpoints and API routes provided by these modules. 
   This API description is available for: 
   
   a. `Main Module <https://dtoceanplus.gitlab.io/api-cooperative/dtop-mm.html>`_, 
   b. `DTOceanPlus Project Service <https://dtoceanplus.gitlab.io/api-cooperative/dtop-project.html>`_,
   c. `Catalogues Module <https://dtoceanplus.gitlab.io/api-cooperative/dtop-catalog.html>`_. 


2. The documentation describing the source code of the module itself, extracted from the docstrings of the code and converted into a technical reference manual using Sphinx. This is divided into sections on the business logic and on the service layer. The business logic includes descriptions of the database models and calculation methods. The service layer details the Flask routes and functions that implement the API.

.. Note::
   
   To be developed by OCC
