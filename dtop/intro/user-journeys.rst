
.. _dtop-user-journeys:

Use cases and user journeys for the DTOceanPlus suite of tools (Design, Assess, Innovate)
==========================================================================================

There are many potential use cases for the DTOceanPlus suite of tools, with corresponding
user journeys between the different modules that have been designed to permit flexibility of use.
Three high-level use cases of the tools could be summarised in terms of the activities:
Design, Assess, and Innovate (although not necessarily in this order).

Design
    Assisting with developing an optimal design of a subsystem, device, or array.

Assess
    Either (1) assessing the performance of a subsystem, device, or array in the context of
    a site and project, or (2) assessing the status of a technology's development.

Innovate
    Providing tools to facilitate structured innovation of both new concepts 
    and improvements to existing technology.


.. figure:: ../figures/tools-loop-v4.svg
    :align: center

    Linkages between the tools showing main flows of information

The user can start this Design, Assess, Innovate loop in several places

- Stage Gate tool to assess technology development stage
- Structured Innovation, to facilitate innovation of new/improved concepts
- Deployment Design tools to design an optimised device/array deployment
    
This example uses the latter.

1.	Sequentially using the seven **Deployment Design tools**, the user can design an optimised array layout. Providing design choices and project details to: 
    
    - Characterise the site and the machine prime-mover
    - Calculate the energy captured by the device, transformed by the PTO, and delivered to shore
    - Design moorings and foundations to keep the device on station, and 
    - Plan logistical and marine operations for the project. 
    
    This gives design parameters including array layout and configurations, design of subsystems, a bill of materials, and the hierarchy of component connections.
    
2.	Then, using the four **Assessment tools** in any order, assess these design parameters. This provides a set of benchmarks or assessment metrics for the design.
3.	They can check the stage of their technology deveopment using the **Stage Gate tool**, by providing details of their technology and development pathway along with the assessment metrics. This results in a Stage gate metrics assessment report.
4.	This may highlight a need for innovation. So, they use the **Structured Innovation tool** to guide the innovation process and identify potential areas for new or improved concepts.

The loop can then be completed by updating the design with the innovation(s). 

Additionally, the Stage Gate tool can provide appropriate complexity levels to run each of the Deployment tools, given the stage of development. So the user can be guided to use the Deployment Design Tools after the Stage Gate tool.





