.. _dtop-intro:

=============================================
Intoduction to the DTOceanPlus suite of tools
=============================================

This section gives an introduction to the DTOceanPlus suite of tools. 
See also the recorded `training material on the DTOceanPlus website <https://www.dtoceanplus.eu/Publications/Training>`_ 

.. toctree::
   :maxdepth: 1

   what-is-dtoceanplus
   user-journeys
   data-requirements
   using-dtop-design-tools
   glossary


      

