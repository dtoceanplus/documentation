.. _what-is-dtoceanplus:


What is DTOceanPlus and who should use it?
====================================================

.. contents::
    :local:

High level overview of capabilities and architecture
-----------------------------------------------------

DTOceanPlus is a modular suite of tools that can either be run together, or independently in standalone mode. 

- In the *integrated mode*, the tools are run sequentially, building up the design of the ocean energy project. 
  It is noted that all previous tools in the chain need to be used in integrated mode, it is not currently possible to skip any of the design tools. 
- In *standalone mode*, the user will need to provide all input data that would normally come from other modules in the suite, in the format of the previous modules' output.
  The tools can all be run in standalone mode from the main module interface, it is also possible to just install some of the modules completely independently and access them directly.
  
The tools are accessed via a web browser interface, or an application programming interface (API), and can be installed either on a server or locally on your computer.

At a high level the tools comprise 4 components, with key outputs as follows:

-	**Structured Innovation tool**, to give potential areas of improvements 
-	**Stage Gate tool**, which produces a stage gate metrics assessment report
-	**Deployment Design tools**, which output design parameters including Array layout and configurations, Design of subsystems, Bill of materials, etc.
-	**Assessment Design tools**, that result in Assessment benchmarks.

The tools are accessed via a *Main Module* and they are supported by Data Management tools of **Catalogues** and a **Digital Representation**, which maintain the underlying data for ocean energy projects and allows sharing of design information.

.. figure:: ../figures/dtoceanplus_tools2.svg
    :align: center
    :width: 75%

    Schematic showing main DTOceanPlus tools

The DTOceanPlus suite of tools support the entire technology innovation and advancement process from concept, through development, to deployment. 
They consider wave and tidal-stream energy converters, both fixed and floating devices, at the level of array, single device, and selected individual sub-systems. 
The tools are designed to work at three levels of complexity, with fewer inputs and simpler data at low complexity allowing a quicker analysis that can be used at early stage. 
At high complexity there are more complex data requirements and longer computational time, to provide more accurate results. 
More information is given in :ref:`dtop-levels-of-complexity`.

.. figure:: ../figures/levels-of-complexity.svg
    :alt: Diagram showing low, medium, high complexity
    :align: center

    Levels of complexity within DTOceanPlus


Who should use DTOceanPlus?
-----------------------------

DTOceanPlus is designed to be useful to a wide range of users. These can be split into three main categories:

1. **Technology Developers** – focusing on developing their specific device/technology.
2. **Project Developers** – focusing on deploying devices/arrays commercially.
3. **Public & Private Investors** – with largely overlapping requirements of understanding financial implications in support of the first two users and development of the sector

Other users, such as certification bodies or academics, will largely be acting in one or more of these capacities. For more details, see :ref:`dtop-use-cases`.

