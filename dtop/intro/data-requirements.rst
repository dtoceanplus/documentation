.. _dtop-data-requirements:

*****************************************************
Overview of data requirements for running DTOceanPlus
*****************************************************

As with any design tool, DTOceanPlus requires a set of inputs to run. 
These comprise user choices, data inputs, and items from the DTOceanPlus catalogues.
This page gives an overview of the data inputs required to run the tools.

.. contents::
   :local:
   :depth: 1



Summary of input data types 
===========================

Site data
---------

Need to represent the environmental conditions and bathymetry of the deployment site. 
Either by uploading data, or by selecting an example reference site in the Site Characterisation tool.  

In the Energy Capture tool, the layout of the individual devices in an array can be optimised. 
Alternatively, a user specified layout can be modelled.

The electrical connection type can be selected in Energy Delivery, 
which then calculates an optimised solution using catalogues of reference components. 

Device data
------------

Basic parameters about the device are input in the Machine Characterisation tool.
Specific details of the electrical systems (gearbox, generator, control, etc) are modelled 
in Energy Transformation.

Mooring and foundation details can be specified or calculated in the Station Keeping module, 
using catalogues of reference components. 

Project data
-------------

Logistical operations to install, operate & maintain, and decommission the project are modelled in the 
Logistics and Marine Operations tool, using a catalogue of reference vessesl and typical operations.

Economic and financial parameters for the project can be entered in the System Lifetime Costs tool, 
and compated to benchmark values stored in the catalogues.



.. Note:: To be developed futher

.. maybe use a csv table for individual modules/complexities/other?

Sample data based on RM1 & RM3
==============================

Sample data to run the US DoE Reference Models [RMP]_ RM1 tidal & RM3 wave is 
included in the `RM1 documentation repository  
<https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/RM1>`_
and `RM3 documentation repository  
<https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/RM3>`_.
Input for running the tools both as an integrated suite, and running individual
modules in standalone mode are provided.

To use the integrated suite of tools two spreadsheets are provided sumarising the inputs for each tool

- ``RM1\RM1.xlsx`` for the 1.1MW twin rotor fixed tidal turbine
- ``RM3\RM3.xlsx`` for the 300kW floating wave energy convertor
- Other required input files are referenced in these spreadsheets, stored in the same directory.


To run individual modules in standalone mode requires all input data to be provided by the user.

Examples developed for the verification of the standalone tools are provided in subdirectories of 
`RM1 standalone <https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/RM1%20standalone/ED>`_  and `RM3 standalone <https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/RM3%20standalone/ED>`_ by module abbreviation. 
Only data for ED module is available initially. 

Sample data for INORE
=====================

As an example, we have prepared three Case Studies for the INORE workshop:

- a `Tidal case (integrated) <https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/INORE/Tidal>`_ in which we run from SC to SK Deployment modules, and the Assessment tools SPEY and SLC.

- a  `Wave case (standalone SK) <https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/INORE/Wave_SK_standalone>`_

- a `Wave case (integrated) <https://gitlab.com/dtoceanplus/documentation/-/tree/master/sample_module_inputs/INORE/Wave>`_ in which we run from SC to ED Deployment modules.

Input for running the tools are provided.

- Other required input files are referenced in the pdf documents, stored in the same directory.

References
==========

.. [RMP] Reference Model Project (RMP), https://energy.sandia.gov/programs/renewable-energy/water-power/projects/reference-model-project-rmp/ 

