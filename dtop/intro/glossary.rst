.. _glossary:

*****************************************
Glossary of key terms used in DTOceanPlus
*****************************************

.. do these get split up into sections if there are many?

.. Note:: Draft in devlopment, some could be refined, what else needs to be added?

Suite of Tools
--------------

Over-arching term for all the tools in DTOceanPlus.

Module/Tool
-----------

Both terms are used to refer the tools within the DTOceanPlus suite as these have been developed in a modular fashion.

.. _glossary-project:

Project
-------

The **project** is the main concept of DTOceanPlus suite. 
It regroups all the data related to the "real project" conducted by the user.
A project within DTOceanPlus defines the *Site* and *Machine* being considered, and can include one or more *Studies*.

.. _glossary-study:

Study
-----

A project is composed of one or several **studies**, this is the term used for a design case of an ocean energy technology at a deployment site that can be independently managed within the tools.

In the frame of the study, the user will use one or several tools to design the solution and then assess it.

The user can use the studies to test different design and compare them.


Forking a Study
===============

Creating a new analysis using some elements of a previously completed study

Site
----

A location on the globe to deploy a device or array of devices. This includes environmental data (wave/tidal/wind), seabed bathymetry (lease/export areas), and is managed in the :ref:`Site Characterisation <sc-home>` module.

Machine
-------

A fixed/floating WEC/TEC used in the design and analyis. This is managed in the :ref:`Machine Characterisation <mc-home>` module, with general parameters, dimensions, and model information regarding performance characteristics.

Entity
------
    
Within a *Study*, each module has an *Entity* that represents the instanciation of a module in the frame of a study.

It contains all of the data (inputs, design choices, and outputs) for that tool. 

These are created and managed within the Main Module of DTOceanPlus.

Complexity level
----------------

The tools are designed to work with projects and technologies over a range of maturity, from early stage concepts to commercial feasibility & design, using different levels of complexity. More detail is given in the how to :ref:`dtop-levels-of-complexity` guide.

Digital Representation
----------------------

A complete description of the user’s project at a given time. It can be seen as a digital version of the real project and therefore it should contain all the needed information to describe the project. 

The *Digital Representation* is the export of all the main data of a study.
It is uses the JSON file format.
    
.. More detail is given in the :ref:`dr-home` documentation. 

Stages and Stage Gates
----------------------

The key feature of the stage gate design tool is the technology development pathway split up into distinct stages, separated by stage gates. The stage gates are an opportunity for users of the tool to assess the technology and make critical decisions on whether to progress to the next stage. More detail is given in the :ref:`Stage Gate Background and Theory section <sg-explanation>`.

Evaluation Areas
================

The areas in which the user measures the success of ocean energy technology to demonstrate progress and performance.

Stage activities
=================
    
This is a list of the research, development and demonstration activities that should be carried out during the prescribed stages.   

Metrics
=========

The parameters used to evaluate how well a technology performs in the Evaluation Areas. These can be outputs of the Deployment and Assessment tools or user inputs.





