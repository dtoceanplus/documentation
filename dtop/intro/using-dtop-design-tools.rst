.. _using-dtop-design-tools:

********************************************************************
Using the DTOceanPlus design tools - lessons from the EnFAIT project
********************************************************************

.. sidebar::  Note
    
    This content was originally written as part of `EnFAIT <https://enfat.eu>`_ deliverable D10.6 DTOcean: Conclusions, 
    and has been adapted to form part of the DTOceanPlus documentation. 


This section covers the use of the DTOceanPlus Design tools in more detail, based on lessons learnt
from using the tools within the `EnFAIT project <https://www.enfait.eu/>`_. 
As part of this project, researchers from The University of Edinburgh used both the DTOcean and
DTOceanPlus design tools to model the EnFAIT array of Nova Innovation tidal turbines in the Bluemull
Sound, Shetland. 

This section is therefore primarily written from the perspective of designing an array of fixed tidal turbines,
but it does touch on other functionalities. For more information on how to achieve specific tasks,
please refer to the documentation for the individual modules.

An overview of the tools is given first, highlighting a fwe key points.
This is followed by sections covering the use of each module in turn, starting with the Main Module. 

.. contents::
    :local:


..    :maxdepth: 2


Overview
========

Levels of complexity
--------------------

There are (generally) three levels of increasing complexity for each tool, referred to both as 
low/medium/high and 1/2/3, see :ref:`dtop-levels-of-complexity` for more details. 
These are discussed below along with the varying data requirements. 
Using a lower complexity for an earlier module can impact on the level of calculation/results 
performed in later modules in the design chain, something to consider when using the tools. 
The array design is built up sequentially in subsequent modules, so any errors and uncertainties 
are propagated in the following design modules.

Overview of useage
------------------

DTOceanPlus is typically used via the Main Module, which manages projects and studies, 
and provides the starting point for use of the other modules. 
A project within DTOceanPlus refers to a machine (either WEC or TEC) at a deployment site. 
Within this, several studies can be created to assess different options within the other design tools, 
such as array layouts, electrical architecture, or logistical solutions. 
These studies can then be 'forked' to consider changes only to the later modules in the design chain, 
see :ref:`mm-tutorial-compare` for more details.

Once a project is set up in the Main Module, the next steps are to define and characterise both the 
deployment site and the device or machine to be used there. 
Next the 'energy chain' is designed and optimised: i.e. energy captured by the array of devices, 
transformed on board each device to electricity, and then delivered to shore via array and export cables. 
The last two design steps are the moorings and foundations, followed by consideration of 
logistical operations for the installation, O&M, and decommissioning phases of the array. 

There are then a set of four Assessment tools, focusing on different metrics: 

- the array performance and energy yield, :ref:`SPEY <spey-home>`
- the economic perspective of lifetime costs, :ref:`SLC <slc-home>`
- the reliability and other aspects, :ref:`RAMS <rams-home>`
- and lastly the environmental and social impacts, :ref:`ESA <esa-home>`.
 
These assessment tools are fully aligned with the IEA-OES evaluation framework [IEA-OES]_. 

There are also tools for :ref:`structured innovation <si-home>` of new and improved concepts, 
and for :ref:`stage gate assessment <sg-home>` of technology development. 
This section focuses only on the use of the deployment design tools to design an array, 
not the assessment, structured innovation, or stage-gate tools.

Modular and integrated use
--------------------------

Although the DTOceanPlus tools are modular and can be used independently, the key value is added 
by running the whole suite of integrated tools. This is done sequentially, as each module builds up
the design. At present it is not possible to skip one or more modules in the design chain and have 
the user manually input the required information/results. 
In theory, it is possible to run a set of multiple modules all standalone and this could potentially
skip one or more modules. In practice, however, formatting the data correctly and managing the API 
calls between modules will likely make this unworkable. 
This limitation could be addressed by future upgrades, building on the digital representation 
framework [D7.1]_ developed as part of the DTOceanPlus project. 
This is partially implemented within the tools, but this would be a significant amount of development 
work to fully implement and test the complete functionality.



Main Module — setting up a project and study
===============================================

The first step in using DTOceanPlus is to log in to the Main Module and create a new project, 
specifying a project name and selecting the technology to be considered (wave or tidal). 
Each project considers a single deployment site and type of device or machine. 
A previously used site or machine can be used by selecting this via the appropriate button. 
Alternatively, a new site and/or machine can be created at this point.

Once the site and machine are set, the user can create one or more studies; 
using the other tools to model and assess various options or configurations. 
Within these studies, there is the option to 'fork' the study, and then using as a new study, 
choose different options in one of the design modules. 
Modules prior to the fork are common to both modules. 
Changes after the fork only apply in that study but propagate to later modules within that fork.

There are currently no other data requirements for the main module.



Site Characterisation — defining and characterising the deployment site
========================================================================

Within DTOceanPlus Site Characterisation (SC), the parameters that do not vary over time, 
i.e. bathymetry, seabed roughness, and species presence, are referred to as “direct values”. 
Timeseries of the wind, waves, and currents are represented by a series of environmental conditions 
with a corresponding probability. 
These can either be 1D at a single point to represent the site, or 2D grids of points covering the 
site lease area and export cable corridor.

The Site Characterisation module includes nine reference sites with varying levels of wave and tidal 
energy (low, medium, high), as shown in the table below. 
These can be used when the user does not have detailed site information. 
The user can also input any uniform depth instead of using the bathymetry data.

*Site Characterisation example sites (data from running SC)*

+--------------------+--------------------+---------------+-----------------+-------+--------------------+
| Wave resource      | Current resource   | Site name/    | Site centre     | Lease | Water depth        |
| mean/max Hs (m)    | mean/max (m/s)     | location      | coord. (°N,°E)  | area  | min/mean/max (m)   |
|                    |                    |               |                 | (km²) |                    |
+======+======+======+======+======+======+===============+=================+=======+======+======+======+
| Low  | 0.48 | 3.14 | Low  | 0.19 | 0.46 | Saint Brieuc  |  46.60, -2.68   |   7.0 |  6.9 | 10.6 | 14.0 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| Low  | 1.27 | 5.79 | Med  | 0.30 | 0.70 | Saint Nazaire |  47.10, -2.35   |  16.1 | 13.0 | 17.6 | 26.3 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| Low  | 1.01 | 5.85 | High | 0.48 |  1.1 | Calais        |  50.80, +1.25   | 364.9 |  7.8 | 31.6 | 53.7 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| Med  | 0.95 | 5.21 | Low  | 0.04 | 0.09 | West Groix    |  47.77, -3.75   |  16.7 |  0.0 | 21.0 | 41.1 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| Med  | 1.32 | 6.87 | Med  | 0.38 | 0.91 | Île de Ré     |  46.10, -1.41   |  29.0 |  1.1 | 20.6 | 40.3 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| Med  | 1.32 | 7.21 | High | 1.43 | 3.33 | Raz Blanchard |  49.76, -2.00   | 101.9 |  0.0 | 49.5 | 86.0 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| High | 1.84 | 11.79| Low  | 0.02 | 0.04 | Hossegor      |  43.70, -1.58   | 195.7 | 19.1 |  108 |  471 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| High | 1.67 | 10.39| Med  | 0.05 | 0.09 | Arcachon      |  44.53, -1.34   |   60.3|  3.5 | 32.3 | 46.4 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+
| High | 1.39 | 9.40 | High | 1.69 | 3.96 | Fromveur      |  48.44, -5.03   |  13.9 |  0.0 | 33.7 | 69.7 |
+------+------+------+------+------+------+---------------+-----------------+-------+------+------+------+


When creating a new site, the three levels of complexity are:

1.	Choose a reference site, with 1D output parameters at a single point.
2.	Choose a reference site, with 2D grid of output parameters across the site. 
3.	Input data files (1D or 2D) to describe a custom deployment site.

Summary statistics are then calculated and presented within the module; 
these are then used in subsequent modules.

To use the Site Characterisation module at complexity 3, the user must provide data on the site 
conditions, which is processed by the SC module. 

- Geometry of the lease area and export cable corridor (as GIS shapefiles: .shp/.shx/.prj/.dbf), 
  in degrees latitude, longitude to WGS84 spherical coordinates.
- Details of the seabed type, roughness, species, and bathymetry (all as NetCDF files). 
  Optionally, a constant depth can be used instead of uploading a bathymetry.
- Timeseries of environmental conditions of waves, tidal currents, winds, and water levels must be 
  provided in 1D, i.e. varying in time at a single representative point on the site 
  (in csv or NetCDF format).   Environmental conditions can optionally also be provided in 2D, 
  varying both temporally and spatially.

.. Note::
    NetCDF is Network Common Data Form, machine-independent data formats that support the 
    creation, access, and sharing of array-oriented scientific data. 
    https://www.unidata.ucar.edu/software/netcdf/ 
    
    The spatial dimension is nominally 2D, but this is stored as a single dimension of 
    node position in the NetCDF file.

Example geometries and timeseries are provided for a couple of sites. 
There are also default databases for the other parameters, see [D5.2]_ for details. 
Unfortunately, these are very low resolution, at 0°00.25' (~450m) for French coastal waters and 
0°05' (~9km) for the whole world, so are not particularly suitable for modelling tidal channels. 
Therefore the user should supply their own data wherever possible. 
This spatial resolution for species presence data can form a useful first step in assessing the 
potential environmental impacts at a site, although more detailed environmental surveys will 
obviously be required for a full assessment if not already available.



Machine Characterisation — defining the device 
==============================================

The Machine Characterisation (MC) module is used to input general parameters and dimensions 
that are used in subsequent design modules. These do not vary between complexity levels and 
are similar for wave and tidal devices. 

A model is used to define the machine features related to power production, 
which has increasing detail with complexity level for each technology:

- **Wave energy**
  
  1. WEC archetype and capture width ratio (CWR).
  2. WEC archetype, average PTO damping and mooring stiffness, CWR or power matrix.
  3. Detailed WEC parameters, from which the linear potential theory coefficients 
     are estimated using a BEM solver.

- **Tidal energy**
  
  1. Rotor power coefficient (Cp) and number of rotors
  2. power and thrust coefficients (Cp, Ct), number of rotors and distance between,
     cut-in and cut-out velocities.
  3. Matrix of Cp, Ct, number of rotors and distance between, cut-in and cut-out velocities,
     yaw angle or bi-directional option.

The level of complexity for the Energy Capture (EC) module is set by that of MC, 
as the MC model directly relates to the EC calculations.

The module also contains a default database with the RM1 and RM3 tidal and wave energy converters 
from the Reference Model Project [RMP]_, plus generic heaving cylinder and surging barge WECs.

.. Note:: 
    As the device electrical connector type and foundation type are set in Machine Characterisation, 
    a separate project is required to consider alterative options for these parameters.



Energy Capture — calculate and optimise energy captured by the array of devices
===============================================================================

The first step in the energy chain is the hydrodynamic energy captured by the device or array of devices.
This module can either calculate results for a user specified layout, or it can calculate an optimised
array layout of devices. 
The layout is specified in terms of UTM easting, northing projected coordinates 
(see :ref:`dtop-conventions`). Care should be taken that these match the corresponding WGS84 
coordinates from SC used to define the site, noting these are converted internally. 
Several array spacing options and optimisation routines are available within the tool, namely 
rectangular, staggered, and 4-parameter arrays, with either brute force, Monte Carlo, or 
CMA-ES optimisation.

The graphical interface for the Energy Capture model is the same at all complexities. 
The calculation depends on the model set in MC, with increasing accuracy associated with the more 
detailed inputs of higher complexity levels.

The outputs from the tool are the device positions (in the case of an optimised layout), 
the total annual energy captured by the whole array and by each device, 
plus a measure of the array interaction -- the q-factor.



Energy Transformation — design energy transformation steps to electrical output
================================================================================

The energy transformation module is used to design the drivetrain and power conversion. 
It has three steps, each with independent levels of complexity. 
These have increasing data inputs and corresponding higher complexity calculations:

1.	Based on rated power only.
2.	Based on rated power and simple inputs.
3.	Based on rated power, inputs, and detailed model.

The three energy transformation steps are:

A.  Mechanical transformation, considering: 

    - direct drive or gearbox for tidal turbines, and 
    - air turbine, hydraulic, or linear to rotational motion system for wave energy convertors.

B.  Electrical transformation: either using a Squirrel Cage Induction Generator (SCIG),
    or a Permanent Magnet Synchronous Generator (PMSG).
C.  Grid conditioning.

The outputs from the tool are a bill of materials for the PTO, plus the energy/power at each step in the
transformation for the whole array, each device, and each PTO (in the case of multiple PTOs per device).



Energy Delivery — design electrical infrastructure to transmit power to shore
==============================================================================

The third and final step in the energy chain is to design the electrical infrastructure to deliver 
power to the shore. 
The Energy Delivery tool is technology agnostic between wave and tidal devices, 
although it does design umbilical cables for floating devices. 

The module only has two levels of complexity, with level 3 being the same inputs and calculations 
as level 2:

1.	Simplified design with default options, always a radial network with collection point
2.	More complex design with user inputs

The user specifies the cable landing point, optionally sets other parameters for the network, and chooses 
one of the six network types:

- Direct connection to shore for each device
- Radial connection with 1 or more strings of devices
- Radial connection with 1 or more strings of devices connected to a collection point
- Single cluster star
- Multiple cluster star
- Multiple cluster star, each connected to a transmission collection point

The collection point can either be a surface piercing substation where the voltage is increased, 
or a subsea hub with no voltage transformation. 
The cable routes and collection point locations are set by the optimisation algorithm. 
The user can choose the cable installation method, or the tool will calculate this based on the seabed type(s). 
Similarly, the tool will optimise the voltage(s) for the network, or the user can set these in the inputs.

.. Note::
    At present, neither Energy Transformation nor Energy Delivery consider voltage transformation 
    on the device. However, it is possible to manually select the device output voltage in ED. 
    This will give a confirmatory warning that the on-board transformer is not designed.

If the export losses are too high, the tool may suggest the export voltage be increased. 
This can either be achieved by using a network with a collection point that can therefore incorporate 
a substation, and/or by adjusting the device output voltage as discussed above.

Cables and other equipment used is considered from the Electrical Components catalogue, and these can 
be updated as required if the user has more accurate information. 
See :ref:`mm-tutorial-catalogues`.

The Energy Delivery tool considers a series of design options and displays results for the top 3 
ranked by cost of energy for the electrical components. 
Cost proxies are used for the costs of installing cables and collection points within the selection 
of the most cost-effective design, but these proxy costs are not included in the ED results as more
accurate estimates are calculated in the LMO module.

The results from the tool are a bill of materials, layout of the cables and collection point(s),
electrical losses, and real/reactive power delivered to shore.



Station Keeping — design moorings and foundations
=================================================

The Station Keeping module is used to design the moorings or foundations for the devices and any
collection points included in the design. 
For floating devices, it designs the mooring lines and anchors. 
This can also include the design of a 'master structure' -- i.e., a rigid frame moored to the seabed 
to which multiple devices can then be moored. 
For fixed foundations, the module only considers the design of the foundation either gravity base or pile.
It does not model any support structure such as a jacket. 
Note that the preferred foundation type is set within the machine characterisation module, 
and thus two projects are required to compare foundation types.

The three levels of complexity in the station keeping module relate to the detail of the user inputs, with
dimensions for mooring system, anchors and foundations automatically determined for the lower complexity. 
The calculation is the same for each complexity.

When running in integrated mode, most of the inputs come from the previous modules and can be reviewed
(but not edited) in the input pages. 
For the foundation design, the user can use default safety factors, load definition, and dimensions, 
or they can manually input these parameters. 

Gravity base foundations can use concrete or steel, with a user defined density. 
These can either be rectangular, cylindrical, or 'contact points' which is a rotationally symmetrical
3-point frame of the required mass.

Piled foundations can either have an open or closed pile tip. 
At present, the user must manually input the length of pile (or height) above the seabed, 
as this is not automatically calculated. 
It should be set as the water depth plus freeboard required above mean water level.

Only an ultimate limit state (ULS) analysis is completed for fixed structures, with the 100-year 
return period wind, wave, and current data coming from the site characterisation module. 
For moorings, a failure limit state (FLS) analysis is also undertaken.

The outputs comprise a high-level bill of materials, estimate of the environmental impact, 
the design details, and results of the ULS/FLS assessment.



Logistics and Marine Operations — plans for installation, O&M, and decommissioning 
==================================================================================

Lastly, once the array design is complete, the Logistics and Marine Operations module can be used 
to plan the vessels, equipment, and ports used for the installation, O&M, and decommissioning. 
As with Energy Delivery, there are only two levels of complexity, with level 3 being the same 
inputs and calculations as level 2:

1.	Simplified, with fewer and simplified inputs required 
2.	Full complexity

For both levels, the user can consider which of the three lifecycle phases to consider: 
installation, maintenance, and decommissioning (only in conjunction with installation, not separately). 

For the simplified mode, dates consider only the month, not the day, and some options take default values.

In integrated mode, the user should first add the project specific inputs, such as start dates, whether
the device is repaired at port, is the device fully submerged, and if it is towed what draft is required.
The inputs from the other modules should then be fetched in turn, or alternatively the user can upload
these in the required json format. 
The site inputs are added by fetching the data from SC then clicking create to run some pre-processing.
The project inputs can then be saved and locked, to progress to the next step of generating the 
installation and maintenance operations. 
The user can then specify requirements for each phase of operations, including entering a maximum distance
to the ports for installation and maintenance. 
The methods to be considered for each operation should also be reviewed/specified. 
For projects with a pilled foundation, the installation method should be selected, 
but this should be left blank if no piled foundations are used. 
Finally the scheduling of the operations is computed on the calculations page. 
On completion, the results show the operations plan and the scheduling thereof. 

.. tip:: The assessment tools can then be used to review the design metrics, but that is not covered here.

References
=============

.. [IEA-OES] J. Hodges et al., “An International Evaluation and Guidance Framework for Ocean Energy Technology,” IEA-OES, 2021. https://www.ocean-energy-systems.org/publications/oes-documents/guidelines/document/an-international-evaluation-and-guidance-framework-for-ocean-energy-technology/

.. [D7.1] V. Nava et al., “DTOceanPlus D7.1 Standard Data Formats of Ocean Energy Systems,”           DTOceanPlus Consortium, 2019. https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D7.1-Standard-Data-Formats-of-Ocean-Energy-Systems

.. [D5.2] Y. Kervella, “DTOceanPlus D5.2 Site Characterisation - alpha version,” DTOceanPlus Consortium, 2020. https://www.dtoceanplus.eu/Publications/Deliverables/Deliverable-D5.2-Site-Characterisation-alpha-version 

.. [RMP] V. S. Neary et al., “Methodology for design and economic analysis of marine energy conversion (MEC) technologies,” Sandia National Laboratories, Albuquerque, New Mexico, USA, SAND2014-9040, 2014. https://energy.sandia.gov/programs/renewable-energy/water-power/projects/reference-model-project-rmp/