.. _dtop-home:

*********************************
Overall DTOceanPlus documentation
*********************************


This is the documentation for the overall DTOceanPlus suite of design tools. 
More detailed documentation of the individual modules within the suite is avalable via the menu on the left, or linked on the :ref:`documentation index <docs-index-modules>`.


Introduction to the software
----------------------------

- :ref:`What is DTOceanPlus and who should use it? <what-is-dtoceanplus>`
- :ref:`dtop-user-journeys`
- :ref:`dtop-data-requirements`
- :ref:`Glossary of key terms used <glossary>`

  


Tutorials
---------

Step-by-step instructions on using DTOceanPlus for new users. It is suggested to follow these tutorials in order.

I. Showcasing features of the DTOceanPlus suite of tools
========================================================

#. :ref:`dtop-deployment-tools-tutorial`
#. :ref:`dtop-assessment-tools-tutorial`
#. :ref:`dtop-stage-gate-tool-tutorial`
#. :ref:`dtop-structured-innovation-tools-tutorial`

See also the recorded `training material on the DTOceanPlus website <https://www.dtoceanplus.eu/Publications/Training>`_ 

II. Installing the DTOceanPlus suite of tools 
=============================================

- :ref:`installing-dtoceanplus-windows10-tutorial` 
- :ref:`installing-dtoceanplus-linux-macos-tutorial`
 
For installation on a company intranet server, please see the 
`README in the installation repository <https://gitlab.com/dtoceanplus/dtop_inst/-/blob/master/README.md>`_


III. Using the DTOceanPlus suite of tools 
=========================================

**Main Module** (the main entry point to the tools, for managing projects and studies)

#. :ref:`mm-tutorial-users`  
#. :ref:`mm-tutorial-create_project`
#. :ref:`mm-tutorial-compare`
#. :ref:`mm-tutorial-dr`

**Catalog Module** (for managing components and reference data)

#. :ref:`cm-browse-catalogs`
#. :ref:`cm-edit-catalogs`

  
Use of the other tools is described in the relevant section of the documentation (see left menu). 


How-to guides
-------------

There are a `series of how-to guides <dtop-how-to>`_ that show how to achieve specific outcomes using DTOceanPlus. 

Background on the overall suite of tools 
----------------------------------------

- :ref:`dtop-modular-architecture`
- :ref:`dtop-use-cases`
- :ref:`Development of the DTOceanPlus suite of tools <dtoceanplus-development>`,  
  summarising the projects and partners involved in the development.
- :ref:`Market analysis and implementation feasibility of ocean energy <market-analysis-reports>`,
  summarising a set of five reports produced within the project.

API Reference
-------------

The :ref:`API reference <dtop-reference>` section documents the code of the 
Main Module classes, API, and GUI for software development. 
  
.. Note:: Still to  be written. Should also cover the Catalogues and Digital Representation.

.. toctree::
   :titlesonly:
   :maxdepth: 1
   :hidden:

   Introduction to DTOceanPlus <./intro/index>
   ./tutorials/index
   ./how_to/index
   ./explanation/index
   ./reference/index

