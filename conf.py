# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------


# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import builtins
import os
import sys
from unittest.mock import MagicMock

# sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath("sg/src"))
sys.path.insert(0, os.path.abspath("si/src"))
sys.path.insert(0, os.path.abspath("deployment/sc/src"))
sys.path.insert(0, os.path.abspath("deployment/mc/src"))
sys.path.insert(0, os.path.abspath("deployment/ec/src"))
sys.path.insert(0, os.path.abspath("deployment/et/src"))
sys.path.insert(0, os.path.abspath("deployment/ed/src"))
sys.path.insert(0, os.path.abspath("deployment/sk/src"))
sys.path.insert(0, os.path.abspath("deployment/lmo/src"))
sys.path.insert(0, os.path.abspath("assessment/spey/src"))
sys.path.insert(0, os.path.abspath("assessment/slc/src"))
sys.path.insert(0, os.path.abspath("assessment/rams/src"))
sys.path.insert(0, os.path.abspath("assessment/esa/src"))

# The following monkeypatches Python's import mechanism in a really ugly way
# so that we don't need to explicitly define the dependencies of every other
# dtocean+ module.

_import = builtins.__import__


def ignore_failed_imports(name, globals=None, locals=None, fromlist=(), level=0):
    if name in ("Stemmer", "chardet","PIL.Image", "roman", "brotli", "winreg", "unicodedata2", "PIL", "simplejson", "Image", "socks"):
        return _import(name, globals, locals, fromlist, level)
    try:
        m = _import(name, globals, locals, fromlist, level)
        return m
    except ModuleNotFoundError:
        print(f"Couldn't import {name}, replacing with a mock.")
        return MagicMock()


builtins.__import__ = ignore_failed_imports


# -- Project information -----------------------------------------------------

project = "DTOceanPlus"
copyright = "2021, DTOceanPlus consortium"
author = "DTOceanPlus consortium"

# The short X.Y version
version = "1.0"
# The full version, including alpha/beta/rc tags
release = "1.0"


# -- General configuration ---------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "plantweb.directive",
    "sphinx_rtd_theme",
]

numfig = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = ".rst"

# The master toctree document.
master_doc = "index"

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = None

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = None


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

html_logo = "./media/dtoceanplus_logo_white.svg"

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {"logo_only": True}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]


def setup(app):
    app.add_css_file("custom.css")


# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# The default sidebars (for documents that don't match any pattern) are
# defined by theme itself.  Builtin themes are using these templates by
# default: ``['localtoc.html', 'relations.html', 'sourcelink.html',
# 'searchbox.html']``.
#
# html_sidebars = {}


# -- Options for HTMLHelp output ---------------------------------------------

# Output file base name for HTML help builder.
htmlhelp_basename = "DTOceanPlusdocumentationdoc"


# -- Options for LaTeX output ------------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',
    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',
    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',
    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (
        master_doc,
        "DTOceanPlusdocumentation.tex",
        "DTOceanPlus documentation Documentation",
        "DTOceanPlus consortium",
        "manual",
    ),
]


# -- Options for manual page output ------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    (
        master_doc,
        "dtoceanplusdocumentation",
        "DTOceanPlus documentation Documentation",
        [author],
        1,
    )
]


# -- Options for Texinfo output ----------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    (
        master_doc,
        "DTOceanPlusdocumentation",
        "DTOceanPlus documentation Documentation",
        author,
        "DTOceanPlusdocumentation",
        "One line description of project.",
        "Miscellaneous",
    ),
]


# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ["search.html"]


# Dependencies
