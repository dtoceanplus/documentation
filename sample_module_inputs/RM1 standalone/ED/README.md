# DTOceanPlus Energy Delivery module inputs for RM1 in standalone mode

The required inputs to run the RM1 case in standalone mode of Energy Delivery at complexity 2 or 3 are as follows. 

- Site Inputs
	- Lease area bathymetry: RM1_lease_bathymetry.json
	- Export area bathymetry: RM1_export_bathymetry.json

- Device inputs
	- Technology (fixed/floating): fixed
	- Device rated power (kW): 11000
	- Device rated voltage (V): 33000
	- Device connector type (wet-mate/dry-mate): wet-mate

- Array inputs
	- Cable landing point co-ordinates: (1300,7600)
	- Array device layout: RM1_10_devices.json
	- Frequency of occurrence of array power output (%): [0.17,0.15,0.16,0.15,0.14,0.1,0.07,0.04,0.02,0]
	
- Cable configuration inputs
	- Network configuration: Radial
	