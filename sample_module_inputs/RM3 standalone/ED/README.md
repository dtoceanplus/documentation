# DTOceanPlus Energy Delivery module inputs for RM3 in standalone mode

The required inputs to run the RM3 example in standalone mode of Energy Delivery at complexity 2 or 3 are as follows. 

- Site Inputs
	- Lease area bathymetry: RM3_lease_bathymetry_reduced.json
	- Export area bathymetry: RM3_export_bathymetry.json

- Device inputs
	- Technology (fixed/floating): floating
	- Device rated power (kW): 300
	- Device rated voltage (V): 3300
	- Device connector type (wet-mate/dry-mate): wet-mate

- Array inputs
	- Cable landing point co-ordinates: (398675,4518475)
	- Array device layout: RM3_10_devices.json
	- Frequency of occurrence of array power output (%): [0.17,0.15,0.16,0.15,0.14,0.1,0.07,0.04,0.02,0]
	
- Cable configuration inputs
	- Network configuration: Radial with transmission collection point
	