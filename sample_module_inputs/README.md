# Sample module input data for DTOceanPlus 

These directories contain sample input data to run the DTOceanPlus tools, 
based on the US DOE Reference Model Project (RMP), 
https://energy.sandia.gov/programs/renewable-energy/water-power/projects/reference-model-project-rmp/ 

## Integrated suite of tools

To use the integrated suite of tools two spreadsheets are provided sumarising the inputs for each tool

- `RM1\RM1.xlsx` for the 1.1MW twin rotor fixed tidal turbine
- `RM3\RM3.xlsx` for the 300kW floating wave nergy convertor

Other required input files are referenced in these spreadsheets, stored in the same direcory.

## Individual module standalone

To run individual modules in standalone mode requires all input data to be provided by the user.

Examples developed for the verification of the standalone tools are provided in subdiretcories of 
`RM1 standalone`  and `RM3 standalone`. 
Only data for some modules is available initially. 
