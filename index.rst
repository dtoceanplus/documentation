.. figure:: ./media/dtoceanplus_logo.svg
    :align: center
    :width: 50%

.. _docs-index:

DTOceanPlus documentation
=========================

This documentation describes DTOceanPlus; the software that has been developed as part of the `EU-funded DTOceanPlus project <https://www.dtoceanplus.eu/>`_.

**DTOceanPlus is an open-source suite of modular design tools for the selection, development, deployment, and assessment of ocean energy systems.**
The aim of the software is to accelerate the commercialisation of the Ocean Energy sector, focusing on wave and tidal-stream devices.
The tools will support the entire technology innovation and advancement process from concept, through development, to deployment, and will be applicable at a range of technology levels: sub-system, device, and array.

.. Important::  The **DTOceanPlus tools are still in development**, and some functionalities have not been fully tested.
                This documentation is also a work in progress; please make suggestions for other areas to be covered. 


Introduction to DTOceanPlus
---------------------------

DTOceanPlus is the next iteration in the development of open-source design tools for the ocean energy sector. 
Building on the original DTOcean code, DTOceanPlus to provides a set of new and improved tools with a wider scope and greatly expanded functionalities. 

The DTOceanPlus project included additional documentation and testing of the code and tools, including demonstration with real projects of the industrial partners in the consortium. 

The tools are freely available to use and extend under an open-source licence, so anyone can now contribute to further test and improve them. 
Many of the DTOceanPlus project partners have plans for future development and use of the tools. 

There is scope for new functionalities to be added, and for further testing and refinement of existing code. 
This is part of an ongoing improvement process to develop a valuable resource for the whole sector to use. 

The DTOceanPlus tools are run using Docker containers, and are accessed via a web-browser interface.

The documentation first covers :ref:`the overall suite of tools <dtop-home>`. 
The individual tools or modules are summarised in the next section of this page.

For new and prospective users of the tools please refer to the following pages:
  
- :ref:`what-is-dtoceanplus`
- :ref:`dtop-user-journeys`
- High level tutorials on each set of tools:
  :ref:`Deployment Design <dtop-deployment-tools-tutorial>`, 
  :ref:`Assessment <dtop-assessment-tools-tutorial>`, 
  :ref:`Stage Gate <dtop-stage-gate-tool-tutorial>`, and
  :ref:`Structured Innovation tools <dtop-structured-innovation-tools-tutorial>`.

There are :ref:`tutorials <dtop-tutorials>` and :ref:`how to guides <dtop-how-to>` covering 
installing the tools on :ref:`Windows 10 <installing-dtoceanplus-windows10-tutorial>` or :ref:`Linux/macOS <installing-dtoceanplus-linux-macos-tutorial>`, 
then :ref:`dtop-getting-started` using the Main Module interface and using the Catalogue Module to access and manage component data, etc.  
Use of the individual tools is covered in their documentation below.

Development of the DTOceanPlus is ongoing, with the history covered in :ref:`dtoceanplus-development`.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Overall DTOceanPlus <./dtop/index>

.. _docs-index-modules:

Modules within the suite of tools 
---------------------------------

- :ref:`Structured Innovation (SI) <si-home>` for concept creation, selection, and design.

- :ref:`Stage Gate (SG) <sg-home>`, using metrics to measure, assess and guide technology development.

- **Deployment Design (DD) tools**, supporting optimal device and array deployment. 

  - :ref:`Site Characterisation (SC) <sc-home>`, to characterise the site, including metocean, geotechnical, and environmental conditions.
  
  - :ref:`Machine Characterisation (MC) <mc-home>`, to characterise the prime mover.
  
  - :ref:`Energy Capture (EC) <ec-home>`, to characterise the device at an array level.
  
  - :ref:`Energy Transformation (ET) <et-home>`, to design PTO and control solutions.
  
  - :ref:`Energy Delivery (ED) <ed-home>`, to design electrical and grid connection solutions.
  
  - :ref:`Station Keeping (SK) <sk-home>`, to design moorings and foundations solutions.
  
  - :ref:`Logistics and Marine Operations (LMO) <lmo-home>`,  to  design logistical solutions and operations plans related to the installation, operation, maintenance, and decommissioning operations.
  
- **Assessment Design (AD) tools**, used to quantify key parameters and evaluate projects and designs.

  - :ref:`System Performance and Energy Yield (SPEY) <spey-home>`,  to  evaluate  projects  in  terms  of  energy performance.
  
  - :ref:`System Lifetime Costs (SLC) <slc-home>`, to evaluate projects from the economic perspective.
  
  - :ref:`System Reliability, Availability, Maintainability, Survivability (RAMS) <rams-home>`, to evaluate the reliability aspects of a marine renewable energy project
  
  - :ref:`Environmental and Social Acceptance (ESA) <esa-home>`, to evaluate the environmental and social impacts of a given wave and tidal energy projects.

- These are supported by **Data Management tools** that maintain the underlying data for ocean energy projects and allow sharing of design information. These are covered within the :ref:`dtop-home`.

  - *Catalogue Module* to manage components used within the design of an ocean energy project.

  - The *Digital Representation* is a framework to standardise data formats describing an ocean energy design, so that it can be used as a common interchange language among different sector actors.




The DTOceanPlus suite of tools are licensed under the open-source 
`GNU Affero General Public License v3.0 <http://choosealicense.com/licenses/agpl-3.0/>`_.

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   Structured Innovation <./si/docs/index>
   Stage Gate <./sg/docs/index>

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:
   :caption: Deployment Design tools

   Site Characterisation <./deployment/sc/docs/index>
   Machine Characterisation <./deployment/mc/docs/index>
   Energy Capture <./deployment/ec/docs/index>
   Energy Transformation <./deployment/et/docs/index>
   Energy Delivery <./deployment/ed/docs/index>
   Station Keeping <./deployment/sk/docs/index>
   Logistics and Marine Operations <./deployment/lmo/docs/index>

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:
   :caption: Assessment tools  

   System Performance and Energy Yield <./assessment/spey/docs/index>
   System Lifetime Costs <./assessment/slc/docs/index>
   Reliability, Availability, Maintainability, Survivability <./assessment/rams/docs/index>
   Environmental and Social Acceptance <./assessment/esa/docs/index>
   
..   Catalog Module <./cm/docs/index>
..   Digital Representation <./dr/docs/index>
