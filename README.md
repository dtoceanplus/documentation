# DTOceanPlus documentation

## Introduction

This is a repository for the documentation of the suite of DTOceanPlus design tools. 
It divides the documentation into individual sections for each of the DTO+ modules.
Module developers are responsible for the documentation of their own module. 

This repository is the place where all the documentation will be pulled together. 
This main documentation repo reads the **docs** folder from each module repository. 
For now, a structure has been defined and a template created for each of the modules.

The repository is set up to automatically deploy the published documentation on GitLab, using [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).
The published documentation is available at [https://dtoceanplus.gitlab.io/documentation](https://dtoceanplus.gitlab.io/documentation). 


## Structure 

The table of contents for the home page of the documentation is structured as follows

- Overall DTOceanPlus
- Structured Innovation (SI)
- Stage Gate (SG)
- Deployment Design (DD) tools
  - Site Characterisation (SC)
  - Machine Characterisation (MC)
  - Energy Capture (EC)
  - Energy Transformation (ET)
  - Energy Delivery (ED)
  - Station Keeping (SK)
  - Logistics  and  Marine  Operations  (LMO)
- Assessment Design (AD)
  - System  Performance  and  Energy  Yield  (SPEY)
  - System Lifetime Costs (SLC)
  - System Reliability, Availability, Maintainability, Survivability (RAMS)
  - Environmental and Social Acceptance (ESA)

Each module's documentation is then divided into four sections. 

1. **Tutorials** to give step-by-step instructions on using the main features of the tool, aimed at beginners.  

2. **How-to Guides** that show how to achieve specific outcomes using the tool. 

3. A section on **Background and theory** that describes how the module works and aims to give confidence in the tools. 

4. An **API reference** section that documents the code of modules, classes, API, and GUI. 

The overall DTOceanPlus documentation provides an introduction to the suite of tools, and also covers: 

- Main Module (MM),
- Catalogue Module (CM) and
- Digital Representation (DR).

These do not currently have an *API Reference* sub-section. 

## Instructions for developers

The documentation for each module should be saved in the **docs** folder of the repo, at the same level as the **src** folder. 
For example, **Energy Delivery (ED)** should  be :

```
- dtop_ed
├── docs                    # DOCs folder goes here!
├── e2e-cypress                    
├── src                     
├── test                    
├── setup.py                   
└── README.md
```


## Images, diagrams and graphs
There are two main options that should be used to add images, diagrams or graphs to the documentation. 
* Option 1: Use inline PlantUML or Graphviz code in the source of the docs, that will generate the charts (flow charts, UML diagrams etc.) when the docs are built.
* Option 2: Generate any figures or graphs as SVG files, which can then be easily added to your docs repo.
Some examples of both options are being created. 
Note that you will need to add an extension to be able to use inline PlantUML in your docs. 
```python
# conf.py
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_rtd_theme",
    "plantweb.directive",  # <- this needs adding
]
```
The reason for selecting these two options is to avoid lots of large JPG/PNG files in the docs repo, as this would quickly clog up the repo and should ideally be avoided. 
SVG files and PlantUML source code are much more "version-controllable" so if at all possible, we urge you to stick to these two methods. 
There may be some exceptions (for example, photos or screenshots that can't be turned into SVG format), but we hope to minimise the number of these exceptions.

### Installing Sphinx 

To be able to generate local versions of the documentation for your specific module, you will need to install Sphinx, using the following command

```
pip install -U sphinx
```

### Installing Read the Docs theme

The Sphinx project template for each individual module uses the [Read the Docs theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/). 
This theme also needs to be installed, using the following command 

```
pip install sphinx-rtd-theme
```

### Installing Sphinx extensions

The plantuml and graphviz support is via [plantweb](https://plantweb.readthedocs.io/), which saves us needing to install java and plantuml locally. Plantweb is installed with pip as follows:

```
pip install plantweb
```

### Running Sphinx

After copying the appropriate template to your main module repo and installing the required dependencies, you should be able to build the HTML version of your module's documentation immediately. 
Do this by running

```
cd docs
make html
```

The built HTML files should be written to the `/docs/_build` folder. 
Click the `/docs/_build/html/index.html` file to open the home page of your documentation. 

### Automodule example

It is strongly recommended to use `autodoc` and `automodule`, together with **Python docstrings**, to document the API reference section of your module. 
This is very easy to configure (assuming the docstrings have already been written). 
For example, the documentation for the business logic of the Stage Gate studies entity (see `sg/docs/reference/business/stage_gate_studies.rst`) is created with these simple lines:

```
Stage Gate Studies
==================

.. automodule:: dtop_stagegate.business.stage_gate_studies
   :members:
   :undoc-members:
   :show-inheritance:
```

Note that this requires the `/src` folder to be populated with the Python source code of your module (which it will be when working in the main repo of the module). 

